package net.fuhrparkservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import net.fuhrparkservice.dto.FahrzeugDTO;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Kunde;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.PKW;
import net.fuhrparkservice.model.Person;
import net.fuhrparkservice.model.Reservierung;

import org.junit.Test;

public class TestQuery extends AbstractJPATestCase {

	private Fahrzeug fahrzeugItem;

	public void setUp() throws Exception {
		FahrzeugType fahrzeug = new PKW(new Model("VW", "Golf"), 120, 200, 2);
		manager.persist(fahrzeug);
		FahrzeugType fahrzeug2 = new LKW(new Model("Mercedes", "10to"), 120,
				200, 10000);
		manager.persist(fahrzeug2);
		FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
		manager.persist(fahrzeug3);
		Filiale filiale = new Filiale("Muenchen");
		fahrzeugItem = new Fahrzeug(filiale, fahrzeug);
		Filiale stuttgart = new Filiale("Stuttgart");
		fahrzeugItem.setStandort(stuttgart);
		manager.persist(fahrzeugItem);
		Fahrzeug fahrzeugItem2 = new Fahrzeug(stuttgart, fahrzeug3);
		manager.persist(fahrzeugItem2);
		manager.persist(new Filiale("Koeln"));
		Kunde kunde = new Kunde(new Person("Hans", "Mustermann"));
		manager.persist(kunde);
		Kunde kunde2 = new Kunde(new Person("Franz", "M�ller"));
		manager.persist(kunde2);
		manager.persist(new Kunde(new Person("Herbert", "Schmitt")));
		manager.persist(new Kunde(new Person("Ingo", "Meyer")));
		manager.persist(new Kunde(new Person("Mathias", "Mayer")));
		manager.persist(new Kunde(new Person("Michael", "Anst�dt")));
		manager.persist(new Kunde(new Person("Ralf", "Gross")));
		this.createReservierung(kunde, fahrzeugItem, stuttgart, 100,
				new GregorianCalendar(2007, 12, 18, 12, 0),
				new GregorianCalendar(2007, 12, 19, 12, 0));
		this.createReservierung(kunde2, fahrzeugItem, filiale, 110,
				new GregorianCalendar(2008, 11, 22, 12, 0),
				new GregorianCalendar(2008, 11, 23, 18, 0));
		this.createReservierung(kunde, fahrzeugItem2, stuttgart, 220,
				new GregorianCalendar(2008, 07, 12, 12, 0),
				new GregorianCalendar(2008, 07, 14, 12, 0));
		this.createReservierung(kunde2, fahrzeugItem2, stuttgart, 330,
				new GregorianCalendar(2007, 04, 30, 12, 0),
				new GregorianCalendar(2007, 05, 02, 12, 0));
		this.createReservierung(kunde, fahrzeugItem2, filiale, 440,
				new GregorianCalendar(2007, 10, 01, 12, 0),
				new GregorianCalendar(2007, 10, 04, 18, 0));
		manager.flush();
		manager.clear();
	}

	private void createReservierung(Kunde kunde, Fahrzeug fahrzeug,
			Filiale filiale, float preis, GregorianCalendar start,
			GregorianCalendar ende) {
		Reservierung res = new Reservierung(fahrzeug, filiale, filiale, start,
				ende, preis);
		kunde.getReservierungen().add(res);
		manager.merge(kunde);
	}

	

	@Test
	public void testSubQuery() {
		List resultList = manager
				.createQuery(
						"Select k FROM Kunde k WHERE 200 < (Select sum(res.preis) from k.reservierungen res)")
				.getResultList();
		assertEquals(2, resultList.size());
	}
}
