package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	private long id;

	@Override
	public void setUp() throws Exception {
		FahrzeugType fahrezug = new FahrzeugType( "VW", "Golf", 120, 200);
		manager.persist(fahrezug);
		id = fahrezug.getId();
		manager.flush();
		manager.clear();
	}

	@Test
	public void testFind() {
		FahrzeugType fahrzeugType = super.manager.find(FahrzeugType.class, id);
		assertNotNull(fahrzeugType.getId());
	}

}
