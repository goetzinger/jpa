package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2012-01-05T22:08:25.062+0100")
@StaticMetamodel(Filiale.class)
public class Filiale_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Filiale, String> ort;
	public static volatile SetAttribute<Filiale, FahrzeugItem> fahrzeuge;
}
