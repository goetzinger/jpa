package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2012-01-05T22:08:25.094+0100")
@StaticMetamodel(Person.class)
public class Person_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Person, String> vorname;
	public static volatile SingularAttribute<Person, String> nachname;
}
