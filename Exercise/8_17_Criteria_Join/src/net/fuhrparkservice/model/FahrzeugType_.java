package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2012-01-05T22:08:25.060+0100")
@StaticMetamodel(FahrzeugType.class)
public class FahrzeugType_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<FahrzeugType, Model> model;
	public static volatile SingularAttribute<FahrzeugType, Long> ps;
	public static volatile SingularAttribute<FahrzeugType, Long> maxKmH;
}
