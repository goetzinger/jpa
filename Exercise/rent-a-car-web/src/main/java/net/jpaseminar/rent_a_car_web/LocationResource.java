package net.jpaseminar.rent_a_car_web;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.jpaseminar.rent_a_car_service.CarService;
import net.jpaseminar.rent_a_car_service.LocationService;
import net.jpaseminar.rent_a_car_service.model.Car;
import net.jpaseminar.rent_a_car_service.model.Location;

@Named
@RequestScoped
@Path("/locations")
public class LocationResource {
	
	@EJB
	private LocationService service;
	
	//http://localhost:8080/api/locations
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Location> getAll(){
		return service.findAll();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Location insert(Location loc) {
		return service.insert(loc);
	}
	
//	//http://localhost:8080/api/cars/10
//	@GET
//	@Path("/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Car getById(@PathParam("id") long id) {
//		return service.findById(id);
//	}
//	
//	//PUT http://localhost:8080/api/cars/1 --> car
//	@PUT
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{id}")
//	public Car update(@PathParam("id") long id, Car car) {
//		return service.update(id, car);
//	}
//	
//	
//	@DELETE
//	@Path("/{id}")
//	public void remove(@PathParam("id") long id) {
//		service.delete(id);
//	}

}
