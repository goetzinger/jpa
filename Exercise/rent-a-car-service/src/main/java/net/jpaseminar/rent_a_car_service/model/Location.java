package net.jpaseminar.rent_a_car_service.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Location {

	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	private String address;
	
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name="location_id")
	private List<Car> carPool;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Car> getCarPool() {
		return carPool;
	}

	public void setCarPool(List<Car> carPool) {
		this.carPool = carPool;
	}
	
	
	
}
