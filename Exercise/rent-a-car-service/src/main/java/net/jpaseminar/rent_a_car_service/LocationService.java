package net.jpaseminar.rent_a_car_service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import net.jpaseminar.rent_a_car_service.model.Location;

@Stateless
@LocalBean
public class LocationService {

	@PersistenceContext
	private EntityManager manager;

	public List<Location> findAll() {
		return manager.createQuery("SELECT l FROM Location l JOIN FETCH l.carPool", Location.class).getResultList();
	}

	public Location insert(Location l) {
		manager.persist(l);
		return l;
	}

}
