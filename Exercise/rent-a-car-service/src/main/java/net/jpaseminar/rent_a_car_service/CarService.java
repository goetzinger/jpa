/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jpaseminar.rent_a_car_service;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.lang3.StringUtils;

import net.jpaseminar.rent_a_car_service.model.Car;
import net.jpaseminar.rent_a_car_service.model.Car_;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;

/**
 *
 * @author martin
 */
@Stateless
@LocalBean
//@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CarService {

	@PersistenceContext
	private EntityManager entityManager;

	public List<Car> findAll() {
		return entityManager.createQuery("SELECT c FROM Car c", Car.class).getResultList();
	}

	public List<Car> findByFilter(String model, String brand, Integer minHP) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Car> createQuery = criteriaBuilder.createQuery(Car.class);
		Root<Car> carEntity = createQuery.from(Car.class);

		List<Predicate> whereParts = new ArrayList<Predicate>();
		if (StringUtils.isNotBlank(brand))
			whereParts.add(criteriaBuilder.equal(carEntity.get(Car_.brand), brand));
		if (StringUtils.isNotBlank(model))
			whereParts.add(criteriaBuilder.equal(carEntity.get(Car_.model), model));
		if (minHP != null && minHP >= 0)
			whereParts.add(criteriaBuilder.greaterThan(carEntity.get(Car_.hp), minHP));
		CriteriaQuery<Car> select = createQuery.select(carEntity);
		if (!whereParts.isEmpty()) {
			Predicate wholeWhereClause = whereParts.size() == 1 ? whereParts.get(0) : 
				criteriaBuilder.and(whereParts.toArray(new Predicate[whereParts.size()]));
			select.where(wholeWhereClause);
		}
		return entityManager.createQuery(createQuery).getResultList();

	}

	public Car insert(Car car) {
		entityManager.persist(car);
		return car;
	}

	// Car update(int id, Car car)
	public Car update(long id, Car newStateOfCar) {
		Car fromDB = entityManager.find(Car.class, id);

		try {
			BeanUtils.copyProperties(fromDB, newStateOfCar);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return fromDB;
	}

	public boolean delete(long id) {
		try {
			entityManager.remove(entityManager.getReference(Car.class, id));
			return true;
		} catch (EntityNotFoundException e) {
			return false;
		}
	}

	public Car findById(long id) {
		return entityManager.find(Car.class, id);
	}
}
