package net.fuhrparkservice;

import java.util.List;

import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.PKW;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestQuery extends AbstractJPATestCase {

	private Fahrzeug fahrzeugItemVW;

	public void setUp() throws Exception {

		FahrzeugType fahrzeugTypeVW = new PKW(new Model("VW", "Golf"), 120, 200, 2);
		manager.persist(fahrzeugTypeVW);
		FahrzeugType fahrzeug2 = new LKW(new Model("Mercedes", "10to"), 120,
				200, 10000);
		manager.persist(fahrzeug2);
		FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
		manager.persist(fahrzeug3);
		Filiale filiale = new Filiale("Muenchen");
		Fahrzeug fahrzeugBMW = new Fahrzeug(filiale,fahrzeug3);
		manager.persist(fahrzeugBMW);
		fahrzeugItemVW = new Fahrzeug(filiale, fahrzeugTypeVW);
		fahrzeugItemVW.setStandort(new Filiale("Stuttgart"));
		manager.persist(fahrzeugItemVW);
		manager.persist(new Nutzer(new Person("Hans", "Mustermann")));
		manager.persist(new Nutzer(new Person("Franz", "M�ller")));
		manager.persist(new Nutzer(new Person("Herbert", "Schmitt")));
		manager.persist(new Nutzer(new Person("Ingo", "Meyer")));
		manager.persist(new Nutzer(new Person("Mathias", "Mayer")));
		manager.persist(new Nutzer(new Person("Michael", "Anst�dt")));
		manager.persist(new Nutzer(new Person("Ralf", "Gross")));

		Filiale koeln = new Filiale("K�ln");
		manager.persist(koeln);

		manager.flush();
		manager.clear();
	}

	@Test
	public void testForFahrzeugItemInFiliale() throws InterruptedException {
		List<Filiale> filialen = manager.createQuery("FROM Filiale f LEFT JOIN FETCH f.fahrzeuge fah LEFT JOIN FETCH fah.type")
				.getResultList();
		System.out.println("Filiale geholt");
		for (Filiale filiale : filialen) {
		
			for (Fahrzeug item : filiale.getFahrzeuge()) {
				System.out.println(item.getType().getModel().getFabrikat());
			}
		}
	}

}
