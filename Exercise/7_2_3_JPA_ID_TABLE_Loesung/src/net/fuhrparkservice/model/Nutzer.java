package net.fuhrparkservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Nutzer implements Serializable {

	@TableGenerator(name = "MY_GEN", table = "tbl_MyGen", pkColumnName = "PK_COLUMN", valueColumnName = "VALUE_COLUMN", pkColumnValue = "NUTZER_ID", allocationSize = 10)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator="MY_GEN")
	private Long id;
	private String vorname;
	private String nachname;

	public Nutzer() {
		super();
	}

	public Nutzer(String vorname, String nachname) {

		this.setVorname(vorname);
		this.setNachname(nachname);
	}

	public Long getId() {
		return this.id;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getVorname() {
		return vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getNachname() {
		return nachname;
	}

}
