package net.test;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

public class LookupUtility {

	private static InitialContext jndiContext;
	private static String JNDI_PREFIX = "9_1_Fuhrpark_JPA_Integration_Enterprise_Loesung/";

	public static <T> T lookup(Class<T> serviceInterface) {
		return lookup(serviceInterface, computeJNDIName(serviceInterface));
	}

	public static <T> T lookup(Class<T> serviceInterface, String name) {

		try {
			InitialContext initialContext = getInitialContext();
			Object proxy = initialContext.lookup(name);
			return (T) PortableRemoteObject.narrow(proxy, serviceInterface);
		} catch (NamingException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("Cannot find Service " + name, e);
		}
	}

	private static String computeJNDIName(Class serviceInterface) {
		return serviceInterface.getName();
	}

	private static InitialContext getInitialContext() throws NamingException {
		if (jndiContext == null) {
			Properties props = new Properties();
			props.setProperty("java.naming.factory.initial",
					"com.sun.enterprise.naming.SerialInitContextFactory");
			props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");// ur
																			// server
																			// ip
			props.setProperty("org.omg.CORBA.ORBInitialPort", "3700"); // default
																		// is
																		// 3700

			jndiContext = new InitialContext(props);
		}
		return jndiContext;
	}

}
