package net.test;

import javax.ejb.EJBException;

import net.transaction.IRemoteService;
import net.transaction.IRemoteTransactedService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTransacted {

	private IRemoteService service;
	private IRemoteTransactedService transactedService;

	@Before
	public void setUp() throws Exception {
		service = LookupUtility.lookup(IRemoteService.class);
		transactedService = LookupUtility
				.lookup(IRemoteTransactedService.class);
	}

	@Test
	public void testNotTransacted() {
		Assert.assertTrue(service.testNotTransacted());
	}

	@Test
	public void testTransacted() {
		Assert.assertTrue(transactedService.test());
	}

	@Test
	public void testTransactedServiceUsingNonTransacted() {
		try {
			transactedService.testByUsingNotTransactedService();
			Assert.fail("This line should never be executed");
		} catch (EJBException e) {
		}
	}

}
