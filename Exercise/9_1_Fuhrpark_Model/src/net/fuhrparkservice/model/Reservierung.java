package net.fuhrparkservice.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries( { @NamedQuery(name = "Reservierung.getAllReservationsByFabrikat", query = "FROM Reservierung res WHERE res.fahrzeug.type.model.fabrikat =:fabrikat") })
@Entity
@Table(name = "tbl_Reservierung")
public class Reservierung extends AbstractBusinessObject {

	@ManyToOne(optional = false)
	private FahrzeugItem fahrzeug;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar startZeit;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar rueckgabeZeit;
	@ManyToOne(optional = false)
	private Filiale startFiliale;
	@ManyToOne(optional = false)
	private Filiale rueckgabeFiliale;
	private float preis;

	public Reservierung() {
	}

	public Reservierung(String id, FahrzeugItem fahrzeugItem,
			Filiale startFiliale, Filiale rueckgabeFiliale,
			GregorianCalendar start, GregorianCalendar ende, float preis) {
		super(id);
		setFahrzeug(fahrzeugItem);
		setStartFiliale(startFiliale);
		setRueckgabeFiliale(rueckgabeFiliale);
		setStartZeit(start);
		setRueckgabeZeit(ende);
		setPreis(preis);
	}

	public Reservierung(FahrzeugItem fahrzeugItem, Filiale startFiliale,
			Filiale rueckgabeFiliale, GregorianCalendar start,
			GregorianCalendar ende, float preis) {
		super();
		setFahrzeug(fahrzeugItem);
		setStartFiliale(startFiliale);
		setRueckgabeFiliale(rueckgabeFiliale);
		setStartZeit(start);
		setRueckgabeZeit(ende);
		setPreis(preis);
	}

	public float getPreis() {
		return preis;
	}

	public void setPreis(float preis) {
		this.preis = preis;
	}

	public FahrzeugItem getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(FahrzeugItem fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public Calendar getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Calendar startZeit) {
		this.startZeit = startZeit;
	}

	public Calendar getRueckgabeZeit() {
		return rueckgabeZeit;
	}

	public void setRueckgabeZeit(Calendar rueckgabeZeit) {
		this.rueckgabeZeit = rueckgabeZeit;
	}

	public Filiale getStartFiliale() {
		return startFiliale;
	}

	public void setStartFiliale(Filiale startFiliale) {
		this.startFiliale = startFiliale;
	}

	public Filiale getRueckgabeFiliale() {
		return rueckgabeFiliale;
	}

	public void setRueckgabeFiliale(Filiale rueckgabeFiliale) {
		this.rueckgabeFiliale = rueckgabeFiliale;
	}

}
