package net.fuhrparkservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Fahrzeug {

	@Id
	@GeneratedValue
	private long id;
	//TODO n:1 unidrektional
	private FahrzeugType type;
	//TODO n:1 bidirektional
	private Filiale standort;

	public Fahrzeug() {
	}


	public Fahrzeug( Filiale standort, FahrzeugType type) {
		this.standort = standort;
		this.type = type;
	}
	
	public long getId() {
		return id;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		this.standort = standort;
	}

	public Filiale getStandort() {
		return standort;
	}
}
