/**
 * 
 */
package net.fuhrparkservice.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 * @author Thomas G&ouml;tzinger
 * 
 */
@Entity
@IdClass(Nutzer.class)
public class Kunde {

	private String kundennummer;

	@Id
	private String vorname;
	@Id
	private String nachname;

	public Kunde() {
		this("", "");
	}

	/**
	 * Konstruktor der Klasse
	 * 
	 * @param vorname
	 * @param nachname
	 */
	public Kunde(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.setKundennummer(UUID.randomUUID().toString());
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Kunde) {
			Kunde other = (Kunde) obj;
			if (this.nachname.equals(other.nachname))
				return this.vorname.equals(other.vorname);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return nachname.hashCode() + vorname.hashCode();
	}

	public void setKundennummer(String kundennummer) {
		this.kundennummer = kundennummer;
	}

	public String getKundennummer() {
		return kundennummer;
	}

}
