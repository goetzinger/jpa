package net.fuhrparkservice.model;

import java.io.Serializable;

public class Nutzer implements Serializable {

	public String vorname;
	public String nachname;

	public Nutzer() {
		super();
	}

	public Nutzer(String vorname, String nachname) {

		this.vorname = vorname;
		this.nachname = nachname;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Nutzer) {
			Nutzer other = (Nutzer) obj;
			if (this.nachname.equals(other.nachname))
				return this.vorname.equals(other.vorname);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return nachname.hashCode() + vorname.hashCode();
	}

}
