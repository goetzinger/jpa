/**
 * 
 */
package net.fuhrparkservice.dto;

/**
 * @author goetzingert
 *
 */
public class FahrzeugDTO {
	
	public final String modell;
	public final long maxKmH;
	
	public FahrzeugDTO(String modell, long maxKmH) {
		this.modell = modell;
		this.maxKmH = maxKmH;
	}

}
