package net.fuhrparkservice.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public class AbstractBusinessObject implements Serializable{

	@Id
	private String id;
	
	@Column(name="optimisticLocking")
	@Version
	private long version;

	public AbstractBusinessObject() {
		this(UUID.randomUUID().toString());
	}

	public AbstractBusinessObject(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
