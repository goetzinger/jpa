package net.fuhrparkservice;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	@Override
	public void setUp() throws Exception {
		
		FahrzeugType fahrzeug = new FahrzeugType("1", new Model("VW", "Golf"), 120, 200);
		manager.persist(fahrzeug);
		Filiale filiale = new Filiale("1", "Muenchen");
		Set<Fahrzeug> fahrzeuge = new HashSet<Fahrzeug>();
		Fahrzeug fahrzeug2 = new Fahrzeug("1",filiale,fahrzeug);
		fahrzeuge.add(fahrzeug2);
		filiale.setFahrzeuge(fahrzeuge );
		manager.persist(fahrzeug2);
		manager.persist(filiale);
		manager.persist(new Nutzer("1", new Person("1", "Hans", "Mustermann")));
		manager.flush();
		manager.clear();
	}

	@Test public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getId());
	}

	@Test public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, "1").getId());
	}

	@Test public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager.find(Filiale.class, "1").getId());
	}

	@Test public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getModel()
				.getFabrikat());
	}

	@Test public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, "1").getPerson()
				.getVorname());
	}

	@Test public void testOneToManyOfFiliale() {
		Filiale filiale = super.manager.find(Filiale.class, "1");
		manager.clear();
		manager.close();
		assertTrue(filiale.getFahrzeuge()
				.toArray().length > 0);
	}
}
