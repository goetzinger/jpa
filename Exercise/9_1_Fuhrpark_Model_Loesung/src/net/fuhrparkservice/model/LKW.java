package net.fuhrparkservice.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="TBL_LKW")
public class LKW extends Fahrzeug {

	private int maxZuladung;

	public LKW() {
		super();
	}

	public LKW(String id, Model model, long ps, long maxKmH, int maxZuladung) {
		super(id, model, ps, maxKmH);
		this.maxZuladung = maxZuladung;
	}

	public int getMaxZuladung() {
		return maxZuladung;
	}

	public void setMaxZuladung(int maxZuladung) {
		this.maxZuladung = maxZuladung;
	}
}
