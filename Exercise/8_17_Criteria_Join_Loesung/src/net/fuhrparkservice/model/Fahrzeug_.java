package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-08-12T09:51:48.752+0200")
@StaticMetamodel(Fahrzeug.class)
public class Fahrzeug_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Fahrzeug, Model> model;
	public static volatile SingularAttribute<Fahrzeug, Long> ps;
	public static volatile SingularAttribute<Fahrzeug, Long> maxKmH;
}
