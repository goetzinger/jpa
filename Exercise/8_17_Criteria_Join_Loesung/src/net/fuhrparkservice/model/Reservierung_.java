package net.fuhrparkservice.model;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-08-12T09:51:48.786+0200")
@StaticMetamodel(Reservierung.class)
public class Reservierung_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Reservierung, FahrzeugItem> fahrzeug;
	public static volatile SingularAttribute<Reservierung, Calendar> startZeit;
	public static volatile SingularAttribute<Reservierung, Calendar> rueckgabeZeit;
	public static volatile SingularAttribute<Reservierung, Filiale> startFiliale;
	public static volatile SingularAttribute<Reservierung, Filiale> rueckgabeFiliale;
	public static volatile SingularAttribute<Reservierung, Float> preis;
}
