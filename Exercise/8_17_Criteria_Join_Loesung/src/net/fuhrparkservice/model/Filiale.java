package net.fuhrparkservice.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_Filiale")
public class Filiale extends AbstractBusinessObject{

	@Column(nullable = false, length = 100)
	private String orta;
	@OneToMany(mappedBy="standort")
	private Set<FahrzeugItem> fahrzeugen = new HashSet<FahrzeugItem>();

	public Filiale() {
	}

	public Filiale(String id, String ort) {
		super(id);
		this.orta = ort;
	}

	public String getOrt() {
		return orta;
	}

	public void setOrt(String ort) {
		this.orta = ort;
	}

	public Set<FahrzeugItem> getFahrzeuge() {
		return fahrzeugen;
	}

	public void setFahrzeuge(Set<FahrzeugItem> fahrzeuge) {
		this.fahrzeugen = fahrzeuge;
	}

	@Override
	public String toString() {
		return this.orta;
	}


}
