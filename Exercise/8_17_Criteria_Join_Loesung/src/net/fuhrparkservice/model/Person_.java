package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-08-12T09:51:48.780+0200")
@StaticMetamodel(Person.class)
public class Person_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Person, String> vorname;
	public static volatile SingularAttribute<Person, String> nachname;
}
