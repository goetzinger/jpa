package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2013-08-12T09:51:48.758+0200")
@StaticMetamodel(FahrzeugItem.class)
public class FahrzeugItem_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<FahrzeugItem, Fahrzeug> type;
	public static volatile SingularAttribute<FahrzeugItem, Filiale> standort;
	public static volatile ListAttribute<FahrzeugItem, Filiale> standortHistory;
}
