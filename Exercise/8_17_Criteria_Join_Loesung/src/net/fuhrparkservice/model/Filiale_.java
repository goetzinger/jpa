package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-11-21T15:42:57.103+0100")
@StaticMetamodel(Filiale.class)
public class Filiale_ extends AbstractBusinessObject_ {
	public static volatile SingularAttribute<Filiale, String> orta;
	public static volatile SetAttribute<Filiale, FahrzeugItem> fahrzeugen;
}
