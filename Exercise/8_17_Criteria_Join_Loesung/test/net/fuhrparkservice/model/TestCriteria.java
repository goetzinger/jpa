package net.fuhrparkservice.model;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;

import junit.framework.Assert;
import net.fuhrparkservice.AbstractJPATestCase;

import org.junit.Test;

public class TestCriteria extends AbstractJPATestCase {

	
	public void setUp() throws Exception {
		
		Fahrzeug fahrzeug = new PKW("1", new Model("VW", "Golf"), 120, 200, 2);
		manager.persist(fahrzeug);
		Fahrzeug fahrzeug2 = new LKW("2", new Model("Mercedes", "10to"), 120,
				200, 10000);
		manager.persist(fahrzeug2);
		Fahrzeug fahrzeug3 = new PKW("3", new Model("BMW", "323"), 150, 220, 4);
		manager.persist(fahrzeug3);
		Filiale filiale = new Filiale("1", "Muenchen");
		FahrzeugItem fahrzeugItem = new FahrzeugItem("1", filiale, fahrzeug);
		Filiale stuttgart = new Filiale("2", "Stuttgart");
		fahrzeugItem.setStandort(stuttgart);
		manager.persist(fahrzeugItem);
		FahrzeugItem fahrzeugItem2 = new FahrzeugItem("2", stuttgart, fahrzeug3);
		manager.persist(fahrzeugItem2);
		manager.persist(new Filiale("3", "Koeln"));
		Kunde kunde = new Kunde("1", new Person("1", "Hans", "Mustermann"));
		manager.persist(kunde);
		Kunde kunde2 = new Kunde("2", new Person("2", "Franz", "M�ller"));
		manager.persist(kunde2);
		manager.persist(new Kunde("3", new Person("3", "Herbert", "Schmitt")));
		manager.persist(new Kunde("4", new Person("4", "Ingo", "Meyer")));
		manager.persist(new Kunde("5", new Person("5", "Mathias", "Mayer")));
		manager.persist(new Kunde("6", new Person("6", "Michael", "Anst�dt")));
		manager.persist(new Kunde("7", new Person("7", "Ralf", "Gross")));
		this.createReservierung(kunde, fahrzeugItem, stuttgart, 100,
				new GregorianCalendar(2007, 12, 18, 12, 0),
				new GregorianCalendar(2007, 12, 19, 12, 0));
		this.createReservierung(kunde2, fahrzeugItem, filiale, 110,
				new GregorianCalendar(2008, 11, 22, 12, 0),
				new GregorianCalendar(2008, 11, 23, 18, 0));
		this.createReservierung(kunde, fahrzeugItem2, stuttgart, 220,
				new GregorianCalendar(2008, 07, 12, 12, 0),
				new GregorianCalendar(2008, 07, 14, 12, 0));
		this.createReservierung(kunde2, fahrzeugItem2, stuttgart, 330,
				new GregorianCalendar(2007, 04, 30, 12, 0),
				new GregorianCalendar(2007, 05, 02, 12, 0));
		this.createReservierung(kunde, fahrzeugItem2, filiale, 440,
				new GregorianCalendar(2007, 10, 01, 12, 0),
				new GregorianCalendar(2007, 10, 04, 18, 0));
		manager.flush();
		manager.clear();
	}
	
	private void createReservierung(Kunde kunde, FahrzeugItem fahrzeug,
			Filiale filiale, float preis, GregorianCalendar start,
			GregorianCalendar ende) {
		Reservierung res = new Reservierung(fahrzeug, filiale, filiale, start,
				ende, preis);
		kunde.getReservierungen().add(res);
		manager.merge(kunde);
	}
	
	
	@Test
	public void SucheFilialeMitFahrzeugVomFabrikatVW(){
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		//TODO use criteriaBuilder
		CriteriaQuery<Filiale> queryFilialeMitVWs = criteriaBuilder.createQuery(Filiale.class);
		
		Root<Filiale> filiale = queryFilialeMitVWs.from(Filiale.class);
		
		SetJoin<Filiale, FahrzeugItem> fahrzeugItemsDerFiliale = filiale.join(Filiale_.fahrzeugen);
		Join<FahrzeugItem, Fahrzeug> fahrzeugeDerFiliale = fahrzeugItemsDerFiliale.join(FahrzeugItem_.type);
		Path<String> modelAttribute = fahrzeugeDerFiliale.get(Fahrzeug_.model).get(Model_.fabrikat);
		Predicate modelEqualsVW = criteriaBuilder.equal(modelAttribute, "VW");
		
		queryFilialeMitVWs.select(filiale).where(modelEqualsVW).distinct(true);
		
		
		List<Filiale> resultList = manager.createQuery(queryFilialeMitVWs).getResultList();
		Assert.assertEquals(1,resultList.size());
	}
}
