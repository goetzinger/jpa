CREATE TABLE tbl_FahrzeugType(
	id integer primary key generated always as identity
	, fabrikat VARCHAR(255)
	, modell VARCHAR(255)
	, ps NUMERIC(3,0)
	, maxKmh NUMERIC(3,0)
	, mietSatz NUMERIC(4,2));

CREATE TABLE tbl_Filiale (
	id integer primary key generated always as identity,
	ort VARCHAR(255));

CREATE TABLE tbl_Fahrzeug (
	id integer primary key generated always as identity,
	inWerkstatt NUMERIC(1,0),
	fahrgestellNummer VARCHAR(50),
	fahrzeug_fk integer);

CREATE TABLE tbl_Fahrzeug_Filiale_Rel (
	fahrzeug_fk integer
	, filiale_fk integer);
	
CREATE TABLE tbl_PKW(
	id integer primary key generated always as identity,
	tueren NUMERIC(1,0),
	fahrzeug_type_fk integer
);

CREATE TABLE tbl_LKW(
	id integer primary key generated always as identity,
	maxZuladung NUMERIC(3,1),
	fahrzeug_type_fk integer
);

CREATE TABLE tbl_User(
	id integer primary key generated always as identity,
	name VARCHAR(50),
	email VARCHAR(255)
);

CREATE TABLE tbl_Kunde(
	id integer primary key generated always as identity,
	kontonummer VARCHAR(50),
	bankleitzahl VARCHAR(50),
	user_fk integer
);

CREATE TABLE tbl_Reservierung
(
	id integer primary key generated always as identity,
	von TIMESTAMP,
	bis TIMESTAMP,
	filiale_fk integer,
	fahrzeug_fk integer,
	kunde_fk integer
);