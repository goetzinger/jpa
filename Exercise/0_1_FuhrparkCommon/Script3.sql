--<ScriptOptions statementTerminator=";"/>

ALTER TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT DROP CONSTRAINT FK_S0CJ9A7JTR8FPQP6RT36M8XHG;

ALTER TABLE APP.TBL_FAHRZEUG DROP CONSTRAINT FK_BU71CF5U1PRM64F0QG4GDC7N7;

ALTER TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT DROP CONSTRAINT FK_7EC4WWOE14G6PQIIWCDGPFTHE;

ALTER TABLE APP.TBL_PERSON DROP CONSTRAINT SQL130812075933080;

ALTER TABLE APP.TBL_FILIALE DROP CONSTRAINT SQL130812075933050;

ALTER TABLE APP.TBL_NUTZER DROP CONSTRAINT SQL130812075933060;

ALTER TABLE APP.TBL_FAHRZEUG DROP CONSTRAINT SQL130812075933070;

DROP INDEX SYS.SYSVIEWS_INDEX1;

DROP INDEX SYS.SYSALIASES_INDEX1;

DROP INDEX APP.SQL130812075933110;

DROP INDEX SYS.SYSROUTINEPERMS_INDEX3;

DROP INDEX SYS.SYSROLES_INDEX2;

DROP INDEX SYS.SYSCOLPERMS_INDEX3;

DROP INDEX SYS.SYSCOLPERMS_INDEX1;

DROP INDEX SYS.SYSSTATEMENTS_INDEX1;

DROP INDEX SYS.SYSCONSTRAINTS_INDEX2;

DROP INDEX SYS.SYSCOLUMNS_INDEX2;

DROP INDEX SYS.SYSTABLES_INDEX2;

DROP INDEX SYS.SYSSCHEMAS_INDEX2;

DROP INDEX SYS.SYSROLES_INDEX1;

DROP INDEX SYS.SYSFILES_INDEX2;

DROP INDEX APP.SQL130812075933100;

DROP INDEX SYS.SYSPERMS_INDEX1;

DROP INDEX SYS.SYSTABLES_INDEX1;

DROP INDEX APP.SQL130812075933060;

DROP INDEX APP.SQL130812075933080;

DROP INDEX SYS.SYSROUTINEPERMS_INDEX1;

DROP INDEX APP.SQL130812075933130;

DROP INDEX SYS.SYSCONGLOMERATES_INDEX1;

DROP INDEX SYS.SYSCONSTRAINTS_INDEX1;

DROP INDEX SYS.SYSSTATEMENTS_INDEX2;

DROP INDEX SYS.SYSCONGLOMERATES_INDEX2;

DROP INDEX SYS.SYSFILES_INDEX1;

DROP INDEX SYS.SYSSCHEMAS_INDEX1;

DROP INDEX SYS.SYSALIASES_INDEX3;

DROP INDEX SYS.SYSCOLPERMS_INDEX2;

DROP INDEX APP.SQL130812075933070;

DROP INDEX SYS.SYSCOLUMNS_INDEX1;

DROP INDEX SYS.SYSCHECKS_INDEX1;

DROP INDEX SYS.SYSFOREIGNKEYS_INDEX1;

DROP INDEX SYS.SYSALIASES_INDEX2;

DROP INDEX SYS.SYSKEYS_INDEX1;

DROP INDEX SYS.SYSSTATISTICS_INDEX1;

DROP INDEX SYS.SYSPERMS_INDEX3;

DROP INDEX SYS.SYSTRIGGERS_INDEX3;

DROP INDEX SYS.SYSTABLEPERMS_INDEX2;

DROP INDEX SYS.SYSROLES_INDEX3;

DROP INDEX SYS.SYSTABLEPERMS_INDEX3;

DROP INDEX SYS.SYSROUTINEPERMS_INDEX2;

DROP INDEX SYS.SYSDEPENDS_INDEX1;

DROP INDEX SYS.SYSFOREIGNKEYS_INDEX2;

DROP INDEX SYS.SYSSEQUENCES_INDEX1;

DROP INDEX SYS.SYSSEQUENCES_INDEX2;

DROP INDEX SYS.SYSPERMS_INDEX2;

DROP INDEX SYS.SYSTABLEPERMS_INDEX1;

DROP INDEX SYS.SYSCONSTRAINTS_INDEX3;

DROP INDEX APP.SQL130812080141680;

DROP INDEX SYS.SYSCONGLOMERATES_INDEX3;

DROP INDEX SYS.SYSTRIGGERS_INDEX1;

DROP INDEX SYS.SYSDEPENDS_INDEX2;

DROP INDEX APP.SQL130812075933050;

DROP INDEX SYS.SYSTRIGGERS_INDEX2;

DROP TABLE SYS.SYSCOLPERMS;

DROP TABLE SYS.SYSROLES;

DROP TABLE APP.TBL_FAHRZEUG;

DROP TABLE APP.TBL_PERSON;

DROP TABLE SYS.SYSTABLES;

DROP TABLE SYS.SYSFILES;

DROP TABLE APP.TBL_FILIALE;

DROP TABLE SYS.SYSVIEWS;

DROP TABLE SYS.SYSDEPENDS;

DROP TABLE SYS.SYSFOREIGNKEYS;

DROP TABLE SYS.SYSTABLEPERMS;

DROP TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT;

DROP TABLE SYS.SYSCOLUMNS;

DROP TABLE SYS.SYSALIASES;

DROP TABLE SYSIBM.SYSDUMMY1;

DROP TABLE SYS.SYSCONGLOMERATES;

DROP TABLE SYS.SYSKEYS;

DROP TABLE SYS.SYSPERMS;

DROP TABLE APP.TBL_NUTZER;

DROP TABLE SYS.SYSCONSTRAINTS;

DROP TABLE SYS.SYSCHECKS;

DROP TABLE SYS.SYSSCHEMAS;

DROP TABLE SYS.SYSSTATISTICS;

DROP TABLE SYS.SYSSEQUENCES;

DROP TABLE SYS.SYSROUTINEPERMS;

DROP TABLE SYS.SYSSTATEMENTS;

DROP TABLE SYS.SYSTRIGGERS;

CREATE TABLE SYS.SYSCOLPERMS (
		COLPERMSID CHAR(36) NOT NULL,
		GRANTEE VARCHAR(128) NOT NULL,
		GRANTOR VARCHAR(128) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		TYPE CHAR(1) NOT NULL,
		COLUMNS null NOT NULL
	);

CREATE TABLE SYS.SYSROLES (
		UUID CHAR(36) NOT NULL,
		ROLEID VARCHAR(128) NOT NULL,
		GRANTEE VARCHAR(128) NOT NULL,
		GRANTOR VARCHAR(128) NOT NULL,
		WITHADMINOPTION CHAR(1) NOT NULL,
		ISDEF CHAR(1) NOT NULL
	);

CREATE TABLE APP.TBL_FAHRZEUG (
		ID VARCHAR(255) NOT NULL,
		STANDORT_ID VARCHAR(255),
		TYPE_ID VARCHAR(255)
	);

CREATE TABLE APP.TBL_PERSON (
		ID VARCHAR(255) NOT NULL,
		NACHNAME VARCHAR(60),
		VORNAME VARCHAR(50)
	);

CREATE TABLE SYS.SYSTABLES (
		TABLEID CHAR(36) NOT NULL,
		TABLENAME VARCHAR(128) NOT NULL,
		TABLETYPE CHAR(1) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		LOCKGRANULARITY CHAR(1) NOT NULL
	);

CREATE TABLE SYS.SYSFILES (
		FILEID CHAR(36) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		FILENAME VARCHAR(128) NOT NULL,
		GENERATIONID BIGINT NOT NULL
	);

CREATE TABLE APP.TBL_FILIALE (
		ID VARCHAR(255) NOT NULL,
		ORT VARCHAR(100) NOT NULL
	);

CREATE TABLE SYS.SYSVIEWS (
		TABLEID CHAR(36) NOT NULL,
		VIEWDEFINITION LONG VARCHAR NOT NULL,
		CHECKOPTION CHAR(1) NOT NULL,
		COMPILATIONSCHEMAID CHAR(36)
	);

CREATE TABLE SYS.SYSDEPENDS (
		DEPENDENTID CHAR(36) NOT NULL,
		DEPENDENTFINDER null NOT NULL,
		PROVIDERID CHAR(36) NOT NULL,
		PROVIDERFINDER null NOT NULL
	);

CREATE TABLE SYS.SYSFOREIGNKEYS (
		CONSTRAINTID CHAR(36) NOT NULL,
		CONGLOMERATEID CHAR(36) NOT NULL,
		KEYCONSTRAINTID CHAR(36) NOT NULL,
		DELETERULE CHAR(1) NOT NULL,
		UPDATERULE CHAR(1) NOT NULL
	);

CREATE TABLE SYS.SYSTABLEPERMS (
		TABLEPERMSID CHAR(36) NOT NULL,
		GRANTEE VARCHAR(128) NOT NULL,
		GRANTOR VARCHAR(128) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		SELECTPRIV CHAR(1) NOT NULL,
		DELETEPRIV CHAR(1) NOT NULL,
		INSERTPRIV CHAR(1) NOT NULL,
		UPDATEPRIV CHAR(1) NOT NULL,
		REFERENCESPRIV CHAR(1) NOT NULL,
		TRIGGERPRIV CHAR(1) NOT NULL
	);

CREATE TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT (
		FAHRZEUGITEM_ID VARCHAR(255) NOT NULL,
		FILIALE_ID VARCHAR(255) NOT NULL
	);

CREATE TABLE SYS.SYSCOLUMNS (
		REFERENCEID CHAR(36) NOT NULL,
		COLUMNNAME VARCHAR(128) NOT NULL,
		COLUMNNUMBER INTEGER NOT NULL,
		COLUMNDATATYPE null NOT NULL,
		COLUMNDEFAULT null,
		COLUMNDEFAULTID CHAR(36),
		AUTOINCREMENTVALUE BIGINT,
		AUTOINCREMENTSTART BIGINT,
		AUTOINCREMENTINC BIGINT
	);

CREATE TABLE SYS.SYSALIASES (
		ALIASID CHAR(36) NOT NULL,
		ALIAS VARCHAR(128) NOT NULL,
		SCHEMAID CHAR(36),
		JAVACLASSNAME LONG VARCHAR NOT NULL,
		ALIASTYPE CHAR(1) NOT NULL,
		NAMESPACE CHAR(1) NOT NULL,
		SYSTEMALIAS null NOT NULL,
		ALIASINFO null,
		SPECIFICNAME VARCHAR(128) NOT NULL
	);

CREATE TABLE SYSIBM.SYSDUMMY1 (
		IBMREQD CHAR(1)
	);

CREATE TABLE SYS.SYSCONGLOMERATES (
		SCHEMAID CHAR(36) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		CONGLOMERATENUMBER BIGINT NOT NULL,
		CONGLOMERATENAME VARCHAR(128),
		ISINDEX null NOT NULL,
		DESCRIPTOR null,
		ISCONSTRAINT null,
		CONGLOMERATEID CHAR(36) NOT NULL
	);

CREATE TABLE SYS.SYSKEYS (
		CONSTRAINTID CHAR(36) NOT NULL,
		CONGLOMERATEID CHAR(36) NOT NULL
	);

CREATE TABLE SYS.SYSPERMS (
		UUID CHAR(36) NOT NULL,
		OBJECTTYPE VARCHAR(36) NOT NULL,
		OBJECTID CHAR(36) NOT NULL,
		PERMISSION CHAR(36) NOT NULL,
		GRANTOR VARCHAR(128) NOT NULL,
		GRANTEE VARCHAR(128) NOT NULL,
		ISGRANTABLE CHAR(1) NOT NULL
	);

CREATE TABLE APP.TBL_NUTZER (
		ID VARCHAR(255) NOT NULL
	);

CREATE TABLE SYS.SYSCONSTRAINTS (
		CONSTRAINTID CHAR(36) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		CONSTRAINTNAME VARCHAR(128) NOT NULL,
		TYPE CHAR(1) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		STATE CHAR(1) NOT NULL,
		REFERENCECOUNT INTEGER NOT NULL
	);

CREATE TABLE SYS.SYSCHECKS (
		CONSTRAINTID CHAR(36) NOT NULL,
		CHECKDEFINITION LONG VARCHAR NOT NULL,
		REFERENCEDCOLUMNS null NOT NULL
	);

CREATE TABLE SYS.SYSSCHEMAS (
		SCHEMAID CHAR(36) NOT NULL,
		SCHEMANAME VARCHAR(128) NOT NULL,
		AUTHORIZATIONID VARCHAR(128) NOT NULL
	);

CREATE TABLE SYS.SYSSTATISTICS (
		STATID CHAR(36) NOT NULL,
		REFERENCEID CHAR(36) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		CREATIONTIMESTAMP TIMESTAMP NOT NULL,
		TYPE CHAR(1) NOT NULL,
		VALID null NOT NULL,
		COLCOUNT INTEGER NOT NULL,
		STATISTICS null NOT NULL
	);

CREATE TABLE SYS.SYSSEQUENCES (
		SEQUENCEID CHAR(36) NOT NULL,
		SEQUENCENAME VARCHAR(128) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		SEQUENCEDATATYPE null NOT NULL,
		CURRENTVALUE BIGINT,
		STARTVALUE BIGINT NOT NULL,
		MINIMUMVALUE BIGINT NOT NULL,
		MAXIMUMVALUE BIGINT NOT NULL,
		INCREMENT BIGINT NOT NULL,
		CYCLEOPTION CHAR(1) NOT NULL
	);

CREATE TABLE SYS.SYSROUTINEPERMS (
		ROUTINEPERMSID CHAR(36) NOT NULL,
		GRANTEE VARCHAR(128) NOT NULL,
		GRANTOR VARCHAR(128) NOT NULL,
		ALIASID CHAR(36) NOT NULL,
		GRANTOPTION CHAR(1) NOT NULL
	);

CREATE TABLE SYS.SYSSTATEMENTS (
		STMTID CHAR(36) NOT NULL,
		STMTNAME VARCHAR(128) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		TYPE CHAR(1) NOT NULL,
		VALID null NOT NULL,
		TEXT LONG VARCHAR NOT NULL,
		LASTCOMPILED TIMESTAMP,
		COMPILATIONSCHEMAID CHAR(36),
		USINGTEXT LONG VARCHAR
	);

CREATE TABLE SYS.SYSTRIGGERS (
		TRIGGERID CHAR(36) NOT NULL,
		TRIGGERNAME VARCHAR(128) NOT NULL,
		SCHEMAID CHAR(36) NOT NULL,
		CREATIONTIMESTAMP TIMESTAMP NOT NULL,
		EVENT CHAR(1) NOT NULL,
		FIRINGTIME CHAR(1) NOT NULL,
		TYPE CHAR(1) NOT NULL,
		STATE CHAR(1) NOT NULL,
		TABLEID CHAR(36) NOT NULL,
		WHENSTMTID CHAR(36),
		ACTIONSTMTID CHAR(36),
		REFERENCEDCOLUMNS null,
		TRIGGERDEFINITION LONG VARCHAR,
		REFERENCINGOLD null,
		REFERENCINGNEW null,
		OLDREFERENCINGNAME VARCHAR(128),
		NEWREFERENCINGNAME VARCHAR(128)
	);

CREATE UNIQUE INDEX SYS.SYSVIEWS_INDEX1 ON SYS.SYSVIEWS (TABLEID ASC);

CREATE UNIQUE INDEX SYS.SYSALIASES_INDEX1 ON SYS.SYSALIASES (SCHEMAID ASC, ALIAS ASC, NAMESPACE ASC);

CREATE INDEX APP.SQL130812075933110 ON APP.TBL_FAHRZEUGITEM_2_STANDORT (FILIALE_ID ASC);

CREATE INDEX SYS.SYSROUTINEPERMS_INDEX3 ON SYS.SYSROUTINEPERMS (ALIASID ASC);

CREATE INDEX SYS.SYSROLES_INDEX2 ON SYS.SYSROLES (ROLEID ASC, ISDEF ASC);

CREATE INDEX SYS.SYSCOLPERMS_INDEX3 ON SYS.SYSCOLPERMS (TABLEID ASC);

CREATE UNIQUE INDEX SYS.SYSCOLPERMS_INDEX1 ON SYS.SYSCOLPERMS (GRANTEE ASC, TABLEID ASC, TYPE ASC, GRANTOR ASC);

CREATE UNIQUE INDEX SYS.SYSSTATEMENTS_INDEX1 ON SYS.SYSSTATEMENTS (STMTID ASC);

CREATE UNIQUE INDEX SYS.SYSCONSTRAINTS_INDEX2 ON SYS.SYSCONSTRAINTS (CONSTRAINTNAME ASC, SCHEMAID ASC);

CREATE INDEX SYS.SYSCOLUMNS_INDEX2 ON SYS.SYSCOLUMNS (COLUMNDEFAULTID ASC);

CREATE UNIQUE INDEX SYS.SYSTABLES_INDEX2 ON SYS.SYSTABLES (TABLEID ASC);

CREATE UNIQUE INDEX SYS.SYSSCHEMAS_INDEX2 ON SYS.SYSSCHEMAS (SCHEMAID ASC);

CREATE UNIQUE INDEX SYS.SYSROLES_INDEX1 ON SYS.SYSROLES (ROLEID ASC, GRANTEE ASC, GRANTOR ASC);

CREATE UNIQUE INDEX SYS.SYSFILES_INDEX2 ON SYS.SYSFILES (FILEID ASC);

CREATE INDEX APP.SQL130812075933100 ON APP.TBL_FAHRZEUG (STANDORT_ID ASC);

CREATE UNIQUE INDEX SYS.SYSPERMS_INDEX1 ON SYS.SYSPERMS (UUID ASC);

CREATE UNIQUE INDEX SYS.SYSTABLES_INDEX1 ON SYS.SYSTABLES (TABLENAME ASC, SCHEMAID ASC);

CREATE UNIQUE INDEX APP.SQL130812075933060 ON APP.TBL_NUTZER (ID ASC);

CREATE UNIQUE INDEX APP.SQL130812075933080 ON APP.TBL_PERSON (ID ASC);

CREATE UNIQUE INDEX SYS.SYSROUTINEPERMS_INDEX1 ON SYS.SYSROUTINEPERMS (GRANTEE ASC, ALIASID ASC, GRANTOR ASC);

CREATE INDEX APP.SQL130812075933130 ON APP.TBL_FAHRZEUGITEM_2_STANDORT (FAHRZEUGITEM_ID ASC);

CREATE INDEX SYS.SYSCONGLOMERATES_INDEX1 ON SYS.SYSCONGLOMERATES (CONGLOMERATEID ASC);

CREATE UNIQUE INDEX SYS.SYSCONSTRAINTS_INDEX1 ON SYS.SYSCONSTRAINTS (CONSTRAINTID ASC);

CREATE UNIQUE INDEX SYS.SYSSTATEMENTS_INDEX2 ON SYS.SYSSTATEMENTS (STMTNAME ASC, SCHEMAID ASC);

CREATE UNIQUE INDEX SYS.SYSCONGLOMERATES_INDEX2 ON SYS.SYSCONGLOMERATES (CONGLOMERATENAME ASC, SCHEMAID ASC);

CREATE UNIQUE INDEX SYS.SYSFILES_INDEX1 ON SYS.SYSFILES (FILENAME ASC, SCHEMAID ASC);

CREATE UNIQUE INDEX SYS.SYSSCHEMAS_INDEX1 ON SYS.SYSSCHEMAS (SCHEMANAME ASC);

CREATE UNIQUE INDEX SYS.SYSALIASES_INDEX3 ON SYS.SYSALIASES (SCHEMAID ASC, SPECIFICNAME ASC);

CREATE UNIQUE INDEX SYS.SYSCOLPERMS_INDEX2 ON SYS.SYSCOLPERMS (COLPERMSID ASC);

CREATE UNIQUE INDEX APP.SQL130812075933070 ON APP.TBL_FAHRZEUG (ID ASC);

CREATE UNIQUE INDEX SYS.SYSCOLUMNS_INDEX1 ON SYS.SYSCOLUMNS (REFERENCEID ASC, COLUMNNAME ASC);

CREATE UNIQUE INDEX SYS.SYSCHECKS_INDEX1 ON SYS.SYSCHECKS (CONSTRAINTID ASC);

CREATE UNIQUE INDEX SYS.SYSFOREIGNKEYS_INDEX1 ON SYS.SYSFOREIGNKEYS (CONSTRAINTID ASC);

CREATE UNIQUE INDEX SYS.SYSALIASES_INDEX2 ON SYS.SYSALIASES (ALIASID ASC);

CREATE UNIQUE INDEX SYS.SYSKEYS_INDEX1 ON SYS.SYSKEYS (CONSTRAINTID ASC);

CREATE INDEX SYS.SYSSTATISTICS_INDEX1 ON SYS.SYSSTATISTICS (TABLEID ASC, REFERENCEID ASC);

CREATE UNIQUE INDEX SYS.SYSPERMS_INDEX3 ON SYS.SYSPERMS (GRANTEE ASC, OBJECTID ASC, GRANTOR ASC);

CREATE INDEX SYS.SYSTRIGGERS_INDEX3 ON SYS.SYSTRIGGERS (TABLEID ASC, CREATIONTIMESTAMP ASC);

CREATE UNIQUE INDEX SYS.SYSTABLEPERMS_INDEX2 ON SYS.SYSTABLEPERMS (TABLEPERMSID ASC);

CREATE UNIQUE INDEX SYS.SYSROLES_INDEX3 ON SYS.SYSROLES (UUID ASC);

CREATE INDEX SYS.SYSTABLEPERMS_INDEX3 ON SYS.SYSTABLEPERMS (TABLEID ASC);

CREATE UNIQUE INDEX SYS.SYSROUTINEPERMS_INDEX2 ON SYS.SYSROUTINEPERMS (ROUTINEPERMSID ASC);

CREATE INDEX SYS.SYSDEPENDS_INDEX1 ON SYS.SYSDEPENDS (DEPENDENTID ASC);

CREATE INDEX SYS.SYSFOREIGNKEYS_INDEX2 ON SYS.SYSFOREIGNKEYS (KEYCONSTRAINTID ASC);

CREATE UNIQUE INDEX SYS.SYSSEQUENCES_INDEX1 ON SYS.SYSSEQUENCES (SEQUENCEID ASC);

CREATE UNIQUE INDEX SYS.SYSSEQUENCES_INDEX2 ON SYS.SYSSEQUENCES (SCHEMAID ASC, SEQUENCENAME ASC);

CREATE INDEX SYS.SYSPERMS_INDEX2 ON SYS.SYSPERMS (OBJECTID ASC);

CREATE UNIQUE INDEX SYS.SYSTABLEPERMS_INDEX1 ON SYS.SYSTABLEPERMS (GRANTEE ASC, TABLEID ASC, GRANTOR ASC);

CREATE INDEX SYS.SYSCONSTRAINTS_INDEX3 ON SYS.SYSCONSTRAINTS (TABLEID ASC);

CREATE INDEX APP.SQL130812080141680 ON APP.TBL_FAHRZEUG (TYPE_ID ASC);

CREATE INDEX SYS.SYSCONGLOMERATES_INDEX3 ON SYS.SYSCONGLOMERATES (TABLEID ASC);

CREATE UNIQUE INDEX SYS.SYSTRIGGERS_INDEX1 ON SYS.SYSTRIGGERS (TRIGGERID ASC);

CREATE INDEX SYS.SYSDEPENDS_INDEX2 ON SYS.SYSDEPENDS (PROVIDERID ASC);

CREATE UNIQUE INDEX APP.SQL130812075933050 ON APP.TBL_FILIALE (ID ASC);

CREATE UNIQUE INDEX SYS.SYSTRIGGERS_INDEX2 ON SYS.SYSTRIGGERS (TRIGGERNAME ASC, SCHEMAID ASC);

ALTER TABLE APP.TBL_PERSON ADD CONSTRAINT SQL130812075933080 PRIMARY KEY (ID);

ALTER TABLE APP.TBL_FILIALE ADD CONSTRAINT SQL130812075933050 PRIMARY KEY (ID);

ALTER TABLE APP.TBL_NUTZER ADD CONSTRAINT SQL130812075933060 PRIMARY KEY (ID);

ALTER TABLE APP.TBL_FAHRZEUG ADD CONSTRAINT SQL130812075933070 PRIMARY KEY (ID);

ALTER TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT ADD CONSTRAINT FK_S0CJ9A7JTR8FPQP6RT36M8XHG FOREIGN KEY (FILIALE_ID)
	REFERENCES APP.TBL_FILIALE (ID);

ALTER TABLE APP.TBL_FAHRZEUG ADD CONSTRAINT FK_BU71CF5U1PRM64F0QG4GDC7N7 FOREIGN KEY (STANDORT_ID)
	REFERENCES APP.TBL_FILIALE (ID);

ALTER TABLE APP.TBL_FAHRZEUGITEM_2_STANDORT ADD CONSTRAINT FK_7EC4WWOE14G6PQIIWCDGPFTHE FOREIGN KEY (FAHRZEUGITEM_ID)
	REFERENCES APP.TBL_FAHRZEUG (ID);

