package net.fuhrparkservice.model;

import java.util.Calendar;
import java.util.Date;

public class Mitarbeiter extends Nutzer {

	private String abteilung;
	private long zeitkonto;
	private Date letzteEinbuchzeit;

	public Mitarbeiter(String vorname, String nachname, String abteilung) {
		super(vorname, nachname);
		this.abteilung = abteilung;
	}

	public String getAbteilung() {
		return abteilung;
	}

	public void einbuchen() {
		if (letzteEinbuchzeit == null)
			letzteEinbuchzeit = Calendar.getInstance().getTime();
		else
			throw new RuntimeException("Sie sind schon eingebucht!");
	}

	public void ausbuchen() {
		if (letzteEinbuchzeit != null) {
			long beginZeit = letzteEinbuchzeit.getTime();
			long endeZeit = Calendar.getInstance().getTimeInMillis();
			long offset = endeZeit - beginZeit;
			this.zeitkonto += offset;
		}
	}
	
	public float getKontostandAsFloat()
	{
		long zeitkontoInSekunden = zeitkonto / 1000;
		return zeitkontoInSekunden / 3600;
	}

	public String getKontostand() {
		long zeitkontoInSekunden = zeitkonto / 1000;
		long sekunden = zeitkontoInSekunden % 60;
		zeitkontoInSekunden -= sekunden;
		long zeitkontoInMinuten = zeitkontoInSekunden / 60;
		long minuten = zeitkontoInMinuten % 60;
		zeitkontoInMinuten -= minuten;
		long zeitkontoInStunden = zeitkontoInMinuten / 60;
		return zeitkontoInStunden + ":" + zeitkontoInMinuten + ":"
				+ zeitkontoInSekunden;

	}
}
