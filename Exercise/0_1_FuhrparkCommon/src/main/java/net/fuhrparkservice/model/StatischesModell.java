package net.fuhrparkservice.model;

import java.util.ArrayList;

public class StatischesModell {
	
	public static ArrayList<FahrzeugType> fahrzeuge = new ArrayList<FahrzeugType>();
	public static ArrayList<Filiale> filialen = new ArrayList<Filiale>();
	
	static{
		//FAHRZEUGE
		PKW bmw320 = new PKW("bmw","320");
		fahrzeuge.add(bmw320);
		PKW mercedesSLK = new PKW("Mercedes","SLK");
		fahrzeuge.add(mercedesSLK);
		PKW opelInsignia = new PKW("Opel","Insignia");
		fahrzeuge.add(opelInsignia);
		PKW vwPassat = new PKW("VW","Passat");
		fahrzeuge.add(vwPassat);
		PKW fordMondeo = new PKW("Ford","Mondeo");
		fahrzeuge.add(fordMondeo);
		
		//FILIALEN
		Filiale filiale = new Filiale("1","M�nchen");
		Fahrzeug item = new Fahrzeug("1", filiale, bmw320);
		filiale.getFahrzeuge().add(item);
		Fahrzeug itemMerceds = new Fahrzeug("1", filiale, mercedesSLK);
		filiale.getFahrzeuge().add(itemMerceds);
		filialen.add(filiale);
		
		Filiale filialeStuttgart = new Filiale("2","Stuttgart");
		Fahrzeug itemStuttgartOpel = new Fahrzeug("3", filialeStuttgart, bmw320);
		filialeStuttgart.getFahrzeuge().add(itemStuttgartOpel);
		Fahrzeug itemStuttgartVw = new Fahrzeug("4", filialeStuttgart, vwPassat);
		filialeStuttgart.getFahrzeuge().add(itemStuttgartVw);
		Fahrzeug itemStuttgartFord = new Fahrzeug("5", filialeStuttgart, fordMondeo);
		filialeStuttgart.getFahrzeuge().add(itemStuttgartFord);
		filialen.add(filialeStuttgart);
		
	}

}
