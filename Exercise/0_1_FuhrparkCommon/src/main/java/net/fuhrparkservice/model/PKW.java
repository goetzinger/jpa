package net.fuhrparkservice.model;

public class PKW extends FahrzeugType {

	private int tueren;
	

	public PKW(String fabrikat, String modell) {
		super();
		setFabrikat(fabrikat);
		setModell(modell);
	}

	public int getTueren() {
		return tueren;
	}

	public void setTueren(int tueren) {
		this.tueren = tueren;
	}

}
