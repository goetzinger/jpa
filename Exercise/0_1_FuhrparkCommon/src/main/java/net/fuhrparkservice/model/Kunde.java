/**
 * 
 */
package net.fuhrparkservice.model;

import java.util.UUID;

/**
 * @author Thomas G&ouml;tzinger
 *
 */
public class Kunde extends Nutzer{
	
	public String kundennummer;
	
	public Kunde()
	{
		this("","");
	}
	
	/**
	 * Konstruktor der Klasse
	 * @param vorname
	 * @param nachname
	 */
	public Kunde(String vorname, String nachname) {
		super(vorname,nachname);
		this.kundennummer = UUID.randomUUID().toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Kunde)
		{
			Kunde other = (Kunde)obj;
			if(this.nachname.equals(other.nachname))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return nachname.hashCode();
	}
	

}
