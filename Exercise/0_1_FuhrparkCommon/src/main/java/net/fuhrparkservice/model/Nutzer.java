package net.fuhrparkservice.model;

import java.io.Serializable;

public class Nutzer implements Serializable{

	public String vorname;
	public String nachname;
	
	public Nutzer()
	{
		super();
	}
	
	public Nutzer(String vorname, String nachname)
	{

		this.vorname = vorname;
		this.nachname = nachname;
	}

}
