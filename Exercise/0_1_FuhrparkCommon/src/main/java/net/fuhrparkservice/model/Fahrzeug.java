package net.fuhrparkservice.model;

public class Fahrzeug extends AbstractBusinessObject {

	private FahrzeugType type;
	private Filiale standort;

	public Fahrzeug(String id, Filiale standort, FahrzeugType type) {
		super(id);
		this.standort = standort;
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		this.standort = standort;
	}

	public Filiale getStandort() {
		return standort;
	}
}
