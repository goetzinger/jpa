package net.fuhrparkservice.model;

import java.io.Serializable;
import java.util.UUID;

public class AbstractBusinessObject implements Serializable{

	private final String id;

	public AbstractBusinessObject() {
		this(UUID.randomUUID().toString());
	}

	public AbstractBusinessObject(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
