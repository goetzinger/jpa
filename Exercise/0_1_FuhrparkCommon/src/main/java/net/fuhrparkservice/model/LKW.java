package net.fuhrparkservice.model;

public class LKW extends FahrzeugType {

	private int maxZuladung;

	public int getMaxZuladung() {
		return maxZuladung;
	}

	public void setMaxZuladung(int maxZuladung) {
		this.maxZuladung = maxZuladung;
	}
}
