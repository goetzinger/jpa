package net.fuhrparkservice.model;

import java.util.HashSet;
import java.util.Set;

public class Filiale extends AbstractBusinessObject {

	private String ort;
	private Set<Fahrzeug> fahrzeuge = new HashSet<Fahrzeug>();
	
	public Filiale() {
	}
	
	public Filiale(String id, String ort)
	{
		super(id);
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}
	
	@Override
	public String toString() {
		return this.ort;
	}

}
