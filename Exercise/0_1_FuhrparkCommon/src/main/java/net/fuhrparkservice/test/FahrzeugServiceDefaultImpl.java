package net.fuhrparkservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.PKW;

public class FahrzeugServiceDefaultImpl implements IFahrzeugCRUD {
	
	private List<FahrzeugType> fahrzeuge = new ArrayList<FahrzeugType>();
	
	public FahrzeugServiceDefaultImpl() {
		fahrzeuge.add(new PKW("BMW","323"));
		fahrzeuge.add(new PKW("Mercedes","SLR"));
		fahrzeuge.add(new PKW("Toyota","Prius"));
		fahrzeuge.add(new PKW("Opel","Insigna"));
	}

	@Override
	public List<FahrzeugType> getAllFahrzeuge() {
		return this.fahrzeuge;
	}

	@Override
	public FahrzeugType getFahrzeugById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FahrzeugType> getFahrzeugeByFabrikat(String fabrikat) {
		List<FahrzeugType> vws = new ArrayList<FahrzeugType>();
		vws.add(new PKW("VW","Golf"));
		vws.add(new PKW("VW","Passat"));
		return vws;
	}

	@Override
	public void insert(FahrzeugType fahrzeug2Insert) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(FahrzeugType fahrzeug2Update) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub
		
	}

}
