package net.fuhrparkservice.service;

import java.util.List;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;

public interface IVerleihService {

	public List<FahrzeugType> getAngeboteneFahrzeuge();

	public List<Filiale> getFilialen();

	public void setSelectedFahrzeug(String id);

	public void setSelectedFiliale(String id);

	public boolean reserviere() throws ReservationException;

}
