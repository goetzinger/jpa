package net.fuhrparkservice.service;

import javax.ejb.ApplicationException;

@ApplicationException
public class ReservationException extends Exception {

	public ReservationException(String message) {
		super(message);
	}
}
