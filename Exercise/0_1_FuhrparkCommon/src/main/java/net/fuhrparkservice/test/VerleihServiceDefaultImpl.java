package net.fuhrparkservice.service;

import java.util.List;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;

public class VerleihServiceDefaultImpl implements IVerleihService{

	private IFahrzeugService fahrzeugService;
	
	@Override
	public List<FahrzeugType> getAngeboteneFahrzeuge() {
		return getFahrzeugService().getAllFahrzeuge();
	}

	@Override
	public List<Filiale> getFilialen() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSelectedFahrzeug(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSelectedFiliale(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean reserviere() throws ReservationException {
		// TODO Auto-generated method stub
		return false;
	}

	public IFahrzeugService getFahrzeugService() {
		return fahrzeugService;
	}

	public void setFahrzeugService(IFahrzeugService fahrzeugService) {
		this.fahrzeugService = fahrzeugService;
	}
	
	
	

}
