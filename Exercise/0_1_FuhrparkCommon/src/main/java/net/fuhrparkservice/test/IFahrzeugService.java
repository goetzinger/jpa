package net.fuhrparkservice.service;

import java.util.List;

public interface IFahrzeugService {

	public List<net.fuhrparkservice.model.FahrzeugType> getAllFahrzeuge();

}