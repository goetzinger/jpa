package net.fuhrparkservice.service;

import java.util.List;

import net.fuhrparkservice.model.Filiale;

public interface IFilialeCRUD {

	public List<Filiale> getAll();

	public Filiale getFilialeById(String id);
	
	public List<Filiale> getFilialenIn(String ort);
	
	public void insert(Filiale filiale2Insert);
	
	public void update(Filiale filiale2Update);
	
	public void remove(String id); 

}
