package net.fuhrparkservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IRowHandler<T> {

	T mapRow(ResultSet rs) throws SQLException;

}
