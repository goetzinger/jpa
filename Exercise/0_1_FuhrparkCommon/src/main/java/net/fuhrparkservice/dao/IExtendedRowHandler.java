package net.fuhrparkservice.dao;

public interface IExtendedRowHandler<T> extends IRowHandler<T> {
	
	public String insertClause(T o);

}
