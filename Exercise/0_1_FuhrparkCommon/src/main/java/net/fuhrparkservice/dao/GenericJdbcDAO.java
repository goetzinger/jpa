package net.fuhrparkservice.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.ejb.EJBException;
import javax.sql.DataSource;

import org.apache.commons.logging.LogFactory;

public class GenericJdbcDAO {

	private DataSource dataSource;
	private Connection connection;

	public GenericJdbcDAO(DataSource ds) {
		this.dataSource = ds;
	}
	

	
	public void close()
	{
		if(this.connection != null)
		{
			try {
				connection.close();
			} catch (SQLException e) {
				LogFactory.getLog(this.getClass()).error("Cannot close Connection",e);
			}
		}
	}

	public <T> List<T> queryForList(String queryString,
			ResultSetHandler<T> resultSetHandler, IRowHandler<T> rowHandler) {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			connection = this.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(queryString);
			return resultSetHandler.convertRowsToEntities(rs, rowHandler);
		} catch (SQLException e) {
			LogFactory.getLog(this.getClass()).error(e);
			throw new EJBException(e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogFactory.getLog(this.getClass()).error(e);
				throw new EJBException(e);
			}
		}
	}

	public <T> T find(String queryString, ResultSetHandler<T> resultSetHandler,
			IRowHandler<T> rowHandler) {
		Connection connection = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			connection = this.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(queryString);
			return resultSetHandler.convertToSingleEntity(rs, rowHandler);
		} catch (SQLException e) {
			LogFactory.getLog(this.getClass()).error(e);
			throw new EJBException(e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (statement != null)
					statement.close();
			} catch (SQLException e) {
				LogFactory.getLog(this.getClass()).error(e);
				throw new EJBException(e);
			}
		}
	}
	
	public Connection getConnection() {
		if(connection == null)
			try {
				connection = dataSource.getConnection();
			} catch (SQLException e) {
				throw new EJBException(e);
			}
		return connection;
	}

}
