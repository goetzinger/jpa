package net.fuhrparkservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.fuhrparkservice.model.Fahrzeug;

public class FahrzeugItemRowHandler implements IRowHandler<Fahrzeug> {

	@Override
	public Fahrzeug mapRow(ResultSet rs) throws SQLException {
		new Fahrzeug(rs.getString("id"),null,null);
		return null;
	}

}
