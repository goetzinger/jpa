package net.fuhrparkservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.fuhrparkservice.model.Filiale;

public class FilialeRowHandler implements IRowHandler<Filiale> {

	@Override
	public Filiale mapRow(ResultSet rs) throws SQLException {
		return new Filiale(rs.getString("id"), rs.getString("ort"));

	}

}
