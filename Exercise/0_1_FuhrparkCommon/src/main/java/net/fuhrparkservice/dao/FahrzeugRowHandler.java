package net.fuhrparkservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.fuhrparkservice.model.FahrzeugType;

public class FahrzeugRowHandler implements IRowHandler<FahrzeugType> {

	@Override
	public FahrzeugType mapRow(ResultSet rs) throws SQLException {
		String id = rs.getString("id");
		String modell = rs.getString("modell");
		String fabrikat = rs.getString("fabrikat");
		long ps = rs.getLong("ps");
		long maxKmH = rs.getLong("maxKmh");
		FahrzeugType f = new FahrzeugType(id,fabrikat,modell,ps,maxKmH);
		return f;
	}

}
