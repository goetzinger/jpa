package net.fuhrparkservice.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultSetHandler<T> {

	public List<T> convertRowsToEntities(ResultSet rs, IRowHandler<T> rowHandler)
			throws SQLException {
		List<T> toReturn = new ArrayList<T>();
		while(rs.next())
		{
			toReturn.add(rowHandler.mapRow(rs));
		}
		return toReturn;
	}

	public T convertToSingleEntity(ResultSet rs,
			IRowHandler<T> rowHandler)
			throws SQLException {
		while(rs.next())
		{
			return rowHandler.mapRow(rs);
		}
		return null;
	}

}
