package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	private FahrzeugType fahrzeugType;
	private Filiale filiale;
	private Nutzer nutzer;

	@Override
	public void setUp() throws Exception {
		
		fahrzeugType = new FahrzeugType(new Model("VW", "Golf"), 120, 200);
		manager.persist(fahrzeugType);
		filiale = new Filiale("Muenchen");
		manager.persist(filiale);
		nutzer = new Nutzer( "Hans", "Mustermann");
		manager.persist(nutzer);
		manager.flush();
		manager.clear();
	}

	@Test public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, fahrzeugType.getId()).getId());
	}

	@Test public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId()).getId());
	}

	@Test public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager.find(Filiale.class, filiale.getId()).getId());
	}

	@Test public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, fahrzeugType.getId()).getModel()
				.getFabrikat());
	}
	
	@Test public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId()).getPerson()
				.getVorname());
	}

}
