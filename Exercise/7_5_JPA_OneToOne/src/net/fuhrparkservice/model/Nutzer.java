package net.fuhrparkservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_Nutzer")
public class Nutzer implements Serializable {

	@Id
	@GeneratedValue
	private long id;
	@Column(length = 50)
	private String vorname;
	@Column(length = 60)
	private String nachname;

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public long getId() {
		return id;
	}

	public Nutzer() {
		super();
	}

	public Nutzer( String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}

}
