package net.fuhrparkservice.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tbl_Filiale")
public class Filiale {

	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false, length = 100)
	private String ort;
	@Transient
	private Set<Fahrzeug> fahrzeuge;

	public Filiale() {
	}

	public Filiale(String ort) {
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}

	@Override
	public String toString() {
		return this.ort;
	}

	public Object getId() {
		return this.id;
	}

}
