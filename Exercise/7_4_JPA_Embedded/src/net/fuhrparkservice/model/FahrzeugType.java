package net.fuhrparkservice.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_FahrzeugType")
public class FahrzeugType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 30, nullable = false)
	private String fabrikat;
	@Column(length = 50, nullable = false)
	private String modell;
	@Column(precision = 4)
	private long ps = 100;
	@Column(precision = 3)
	private long maxKmH;

	public FahrzeugType() {
	}

	public FahrzeugType(String fabrikat, String modell, long ps,
			long maxKmH) {
		this.fabrikat = fabrikat;
		this.modell = modell;
		this.ps = ps;
		this.maxKmH = maxKmH;
	}

	public long getId() {
		return id;
	}

	public void setFabrikat(String fabrikat) {
		this.fabrikat = fabrikat;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	public String getFabrikat() {
		return fabrikat;
	}

	public String getModell() {
		return modell;
	}

	public long getPs() {
		return ps;
	}

	public void setPs(long ps) {
		this.ps = ps;
	}

	public long getMaxKmH() {
		return maxKmH;
	}

	public void setMaxKmH(long maxKmH) {
		this.maxKmH = maxKmH;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof FahrzeugType) {
			FahrzeugType other = (FahrzeugType) obj;
			if (this.fabrikat.equals(other.fabrikat)
					&& this.modell.equals(other.modell))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = fabrikat.hashCode();
		hashCode *= modell.hashCode();
		return hashCode;
	}

	@Override
	public String toString() {
		return this.fabrikat + " : " + this.modell;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream objout = new ObjectOutputStream(out);
			objout.writeObject(this);
			objout.flush();
			ByteArrayInputStream in = new ByteArrayInputStream(
					out.toByteArray());
			ObjectInputStream obIn = new ObjectInputStream(in);
			Object toReturn = obIn.readObject();
			obIn.close();
			in.close();
			out.close();
			objout.close();
			return toReturn;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		}

	}

}
