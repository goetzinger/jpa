package net.fuhrparkservice.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_FahrzeugType")
public class FahrzeugType {

	@Id
	private String id;
	@Embedded
	private Model model;
	@Column(precision = 4)
	private long ps = 100;
	@Column(precision = 3)
	private long maxKmH;

	public FahrzeugType() {
	}

	public FahrzeugType(String id, Model model, long ps, long maxKmH) {
		this.id = id;
		this.ps = ps;
		this.maxKmH = maxKmH;
		this.model = model;
	}

	public String getId() {
		return id;
	}

	public long getPs() {
		return ps;
	}

	public void setPs(long ps) {
		this.ps = ps;
	}

	public long getMaxKmH() {
		return maxKmH;
	}

	public void setMaxKmH(long maxKmH) {
		this.maxKmH = maxKmH;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (int) (maxKmH ^ (maxKmH >>> 32));
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + (int) (ps ^ (ps >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FahrzeugType other = (FahrzeugType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (maxKmH != other.maxKmH)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (ps != other.ps)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getModel().toString();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream objout = new ObjectOutputStream(out);
			objout.writeObject(this);
			objout.flush();
			ByteArrayInputStream in = new ByteArrayInputStream(out
					.toByteArray());
			ObjectInputStream obIn = new ObjectInputStream(in);
			Object toReturn = obIn.readObject();
			obIn.close();
			in.close();
			out.close();
			objout.close();
			return toReturn;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		}

	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Model getModel() {
		return model;
	}

}
