package net.fuhrparkservice.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Person
 * 
 */
@Entity
@Table(name="tbl_person")
public class Person implements Serializable {

	@Id
	private String id;
	@Column(length = 50)
	private String vorname;
	@Column(length = 60)
	private String nachname;

	public Person() {
		super();
	}
	

	public Person(String id, String vorname, String nachname) {
		super();
		this.id = id;
		this.vorname = vorname;
		this.nachname = nachname;
	}


	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getId() {
		return id;
	}


}
