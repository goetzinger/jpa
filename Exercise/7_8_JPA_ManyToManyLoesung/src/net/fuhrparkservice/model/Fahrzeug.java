package net.fuhrparkservice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fahrzeug")
public class Fahrzeug {

	@Id
	private String id;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private FahrzeugType type;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Filiale standort;
	@ManyToMany(cascade={CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "standort_historie", joinColumns = { @JoinColumn(name = "fahrzeugItem_id") }, inverseJoinColumns = { @JoinColumn(name = "filiale_id") })
	private List<Filiale> standortHistory = new ArrayList<Filiale>();

	public List<Filiale> getStandortHistory() {
		return standortHistory;
	}

	public void setStandortHistory(List<Filiale> standortHistory) {
		this.standortHistory = standortHistory;
	}

	public Fahrzeug() {
	}

	public Fahrzeug(String id, Filiale standort, FahrzeugType type) {
		this.id = id;
		setStandort(standort);
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		standort.getFahrzeuge().add(this);
		if (this.standort != null)
		{
			this.standortHistory.add(this.standort);
			this.standort.getFahrzeuge().remove(this);
		}
		this.standort = standort;
		
	}
	
	

	public Filiale getStandort() {
		return standort;
	}
}
