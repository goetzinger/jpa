package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	@Override
	public void setUp() throws Exception {
		
		FahrzeugType fahrzeug = new FahrzeugType("1", new Model("VW", "Golf"), 120, 200);
		manager.persist(fahrzeug);
		Filiale filiale = new Filiale("1", "Muenchen");
		Fahrzeug fahrzeugItem = new Fahrzeug("1", filiale, fahrzeug);
		fahrzeugItem.setStandort(new Filiale("2", "Stuttgart"));
		manager.persist(fahrzeugItem);
		manager.persist(new Nutzer("1", new Person("1", "Hans", "Mustermann")));
		manager.flush();
		manager.clear();
	}

	@Test public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getId());
	}

	@Test public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, "1").getId());
	}

	@Test public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager.find(Filiale.class, "1").getId());
	}

	@Test public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, "1").getModel()
				.getFabrikat());
	}

	@Test public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, "1").getPerson()
				.getVorname());
	}

	@Test public void testOneToManyOfFiliale() {
		assertTrue(super.manager.find(Filiale.class, "2").getFahrzeuge()
				.toArray().length > 0);
	}

	@Test public void testManyToOneFahrzeugItem() {
		assertNotNull(super.manager.find(Fahrzeug.class, "1").getStandort());
	}

	@Test public void testManyToManyFahrzeugItem() {
		assertTrue(super.manager.find(Fahrzeug.class, "1")
				.getStandortHistory().size() > 0);
	}
}
