package net.transaction;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * Session Bean implementation class TransactedService
 */
@Stateless
@Remote( { IRemoteTransactedService.class })
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TransactedService implements IRemoteTransactedService {

	@EJB(beanInterface = IRemoteService.class)
	private IRemoteService nonTransactedService;

	
	public boolean test() {
		return true;
	}

	public boolean testByUsingNotTransactedService() {
		return nonTransactedService.testNotTransacted();
	}

}
