package net.transaction;

public interface IRemoteTransactedService {
	
	public boolean test();
	
	public boolean testByUsingNotTransactedService();

}
