package net.transaction;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@Remote( { IRemoteService.class })
@Local(IRemoteService.class)
public class NonTransactedService implements IRemoteService {


    @TransactionAttribute(TransactionAttributeType.NEVER)
    public boolean testNotTransacted() {
    	return true;
    }

}
