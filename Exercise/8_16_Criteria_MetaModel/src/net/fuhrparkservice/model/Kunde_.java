package net.fuhrparkservice.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2012-01-05T22:08:25.064+0100")
@StaticMetamodel(Kunde.class)
public class Kunde_ extends Nutzer_ {
	public static volatile SingularAttribute<Kunde, String> kundennummer;
	public static volatile ListAttribute<Kunde, Reservierung> reservierungen;
}
