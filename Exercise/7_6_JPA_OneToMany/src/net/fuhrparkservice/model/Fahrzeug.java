package net.fuhrparkservice.model;

import javax.persistence.Transient;

public class Fahrzeug {

	private long id;
	
	//TODO Unidirektional 
	@Transient
	private FahrzeugType type;
	
	@Transient
	private Filiale standort;



	public Fahrzeug(Filiale standort, FahrzeugType type) {
		this.standort = standort;
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		this.standort = standort;
	}

	public Filiale getStandort() {
		return standort;
	}
}
