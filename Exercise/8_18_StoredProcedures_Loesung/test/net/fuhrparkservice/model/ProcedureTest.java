package net.fuhrparkservice.model;

import static org.junit.Assert.*;

import java.util.Calendar;

import javax.persistence.Query;

import org.junit.Test;

import net.fuhrparkservice.AbstractJPATestCase;

public class ProcedureTest extends AbstractJPATestCase {

	@Test
	public void testNamed() {
		Query query = manager.createNamedQuery(LKW.insertLKWProcedure);
		query.setParameter(1, "2");
		query.setParameter(2, "Toyota");
		query.setParameter(3, "Corolla");
		query.setParameter(4, 110);
		query.setParameter(5, 180);
		query.setParameter(6, 10);
		System.out.println(Calendar.getInstance().getTimeInMillis());
		query.executeUpdate();
		System.out.println(Calendar.getInstance().getTimeInMillis());
		query.setParameter(1, "3");
		System.out.println(Calendar.getInstance().getTimeInMillis());
		query.executeUpdate();
		System.out.println(Calendar.getInstance().getTimeInMillis());
	}

	@Test
	public void test() {
		Query query = manager.createNativeQuery("begin insertLKW (?,?,?,?,?,?); END;");
		query.setParameter(1, "11");
		query.setParameter(2, "Toyota");
		query.setParameter(3, "Corolla");
		query.setParameter(4, 110);
		query.setParameter(5, 180);
		query.setParameter(6, 10);
		System.out.println(Calendar.getInstance().getTimeInMillis());
		query.executeUpdate();
		System.out.println(Calendar.getInstance().getTimeInMillis());
		Query query2 = manager.createNativeQuery("begin insertLKW (?,?,?,?,?,?); END;");
		query2.setParameter(1, "122");
		query2.setParameter(2, "Toyota");
		query2.setParameter(3, "Corolla");
		query2.setParameter(4, 110);
		query2.setParameter(5, 180);
		query2.setParameter(6, 10);
		System.out.println(Calendar.getInstance().getTimeInMillis());
		query2.executeUpdate();
		
		
	}
	

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
