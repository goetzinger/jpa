CREATE OR REPLACE PROCEDURE insertLKW (p_id IN VARCHAR2, p_fabrikat IN VARCHAR2, p_model IN VARCHAR2, p_ps IN NUMBER, p_maxKmh IN NUMBER, p_maxZuladung IN NUMBER)
IS
BEGIN
    insert into TBL_FAHRZEUGTYPE (id, modell, fabrikat, ps, maxkmh)
    values (p_id,p_model,p_fabrikat,p_ps,p_maxkmh);
    
    insert into TBL_LKW (id,maxzuladung) values (p_id,p_maxZuladung);
    
    commit;
END insertLKW;

CREATE OR REPLACE FUNCTION countLKW return INTEGER IS
	lkws integer;
BEGIN
		SELECT count(*) INTO lkws FROM tbl_LKW;
		return lkws;
END countLKW;