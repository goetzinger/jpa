package net.fuhrparkservice.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_LKW")
@org.hibernate.annotations.NamedNativeQuery(resultSetMapping = "myMapping", name = LKW.insertLKWProcedure, query = "begin insertLKW (?,?,?,?,?,?); END;")
@SqlResultSetMapping(name = "myMapping", columns = { @ColumnResult(name = "a") })
public class LKW extends FahrzeugType {

	public static final String insertLKWProcedure = "insertLKWProcedure";
	private int maxZuladung;

	public LKW() {
		super();
	}

	public LKW(String id, Model model, long ps, long maxKmH, int maxZuladung) {
		super(id, model, ps, maxKmH);
		this.maxZuladung = maxZuladung;
	}

	public int getMaxZuladung() {
		return maxZuladung;
	}

	public void setMaxZuladung(int maxZuladung) {
		this.maxZuladung = maxZuladung;
	}
}
