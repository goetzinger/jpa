package net.fuhrparkservice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fahrzeug")
public class Fahrzeug {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private FahrzeugType type;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Filiale standort;
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "tbl_fahrzeugItem_2_standort", joinColumns = { @JoinColumn(name = "fahrzeugItem_id") }, inverseJoinColumns = { @JoinColumn(name = "filiale_id") })
	private List<Filiale> standortHistory = new ArrayList<Filiale>();

	public Fahrzeug() {
	}

	public Fahrzeug(Filiale standort, FahrzeugType type) {
		setStandort(standort);
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		standort.getFahrzeuge().add(this);
		if (this.standort != null) {
			this.standortHistory.add(this.standort);
			this.standort.getFahrzeuge().remove(this);
		}
		this.standort = standort;

	}

	public Filiale getStandort() {
		return standort;
	}

	public List<Filiale> getStandortHistory() {
		return standortHistory;
	}
}
