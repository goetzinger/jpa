package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	private FahrzeugType fahrzeug;
	private Filiale filiale;
	private Fahrzeug fahrzeugItem;
	private Nutzer nutzer;

	@Override
	public void setUp() throws Exception {

		fahrzeug = new FahrzeugType(new Model("VW", "Golf"), 120, 200);
		manager.persist(fahrzeug);
		filiale = new Filiale("Muenchen");
		fahrzeugItem = new Fahrzeug(filiale, fahrzeug);
		manager.persist(fahrzeugItem);
		nutzer = new Nutzer(new Person("Hans", "Mustermann"));
		manager.persist(nutzer);
		manager.flush();
		manager.clear();
	}

	@Test
	public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, fahrzeug.getId())
				.getId());
	}

	@Test
	public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId()).getId());
	}

	@Test
	public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager.find(Filiale.class, filiale.getId())
				.getId());
	}

	@Test
	public void testFindModel() {
		assertNotNull(super.manager.find(FahrzeugType.class, fahrzeug.getId())
				.getModel().getFabrikat());
	}

	@Test
	public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId())
				.getPerson().getVorname());
	}

	@Test
	public void testOneToManyOfFiliale() {
		assertTrue(super.manager.find(Filiale.class, filiale.getId())
				.getFahrzeuge().toArray().length > 0);
	}

	@Test
	public void testManyToOneFahrzeugItem() {
		assertNotNull(super.manager.find(Fahrzeug.class, fahrzeugItem.getId())
				.getStandort());
	}
}
