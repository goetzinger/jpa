package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {
	
	private Long id;

	@Override
	public void setUp() throws Exception {
		
		FahrzeugType fahrzeug = new FahrzeugType("VW","Golf",120,200);
		manager.persist(fahrzeug);
		this.id = fahrzeug.getId();
		manager.flush();
		manager.clear();
	}
	
	@Test public void testFind()
	{
		assertEquals(id,super.manager.find(FahrzeugType.class, id).getId());
	}
	
	

}
