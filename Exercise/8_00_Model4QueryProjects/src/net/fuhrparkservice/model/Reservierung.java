package net.fuhrparkservice.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tbl_Reservierung")
@NamedQueries({
	@NamedQuery(name=Reservierung.FIND_BY_START_FILIALE, query=Reservierung.FIND_BY_START_FILIALE)
})
public class Reservierung extends AbstractBusinessObject {
	
	public static final String PARAM_FILIALE = "filiale";

	public static final String FIND_BY_START_FILIALE = "SELECT r FROM Reservirung r WHERE r.startFiliale = :" + PARAM_FILIALE;

	@ManyToOne(optional = false)
	private Fahrzeug fahrzeug;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar startZeit;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar rueckgabeZeit;
	@ManyToOne(optional = false)
	private Filiale startFiliale;
	@ManyToOne(optional = false)
	private Filiale rueckgabeFiliale;
	private float preis;

	public Reservierung() {
	}

	public Reservierung(Fahrzeug fahrzeugItem, Filiale startFiliale,
			Filiale rueckgabeFiliale, GregorianCalendar start,
			GregorianCalendar ende, float preis) {
		super();
		setFahrzeug(fahrzeugItem);
		setStartFiliale(startFiliale);
		setRueckgabeFiliale(rueckgabeFiliale);
		setStartZeit(start);
		setRueckgabeZeit(ende);
		setPreis(preis);
	}

	public float getPreis() {
		return preis;
	}

	public void setPreis(float preis) {
		this.preis = preis;
	}

	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public Calendar getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Calendar startZeit) {
		this.startZeit = startZeit;
	}

	public Calendar getRueckgabeZeit() {
		return rueckgabeZeit;
	}

	public void setRueckgabeZeit(Calendar rueckgabeZeit) {
		this.rueckgabeZeit = rueckgabeZeit;
	}

	public Filiale getStartFiliale() {
		return startFiliale;
	}

	public void setStartFiliale(Filiale startFiliale) {
		this.startFiliale = startFiliale;
	}

	public Filiale getRueckgabeFiliale() {
		return rueckgabeFiliale;
	}

	public void setRueckgabeFiliale(Filiale rueckgabeFiliale) {
		this.rueckgabeFiliale = rueckgabeFiliale;
	}

}
