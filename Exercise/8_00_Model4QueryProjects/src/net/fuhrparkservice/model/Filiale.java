package net.fuhrparkservice.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "tbl_Filiale")
public class Filiale extends AbstractBusinessObject {

	@Column(nullable = false, length = 100)
	private String ort;
	@OneToMany(mappedBy = "standort")
	@Fetch(FetchMode.SUBSELECT)
	@BatchSize(size=100)
	private Set<Fahrzeug> fahrzeuge = new HashSet<Fahrzeug>();

	public Filiale() {
	}

	public Filiale(String ort) {
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}

	@Override
	public String toString() {
		return this.ort;
	}

}
