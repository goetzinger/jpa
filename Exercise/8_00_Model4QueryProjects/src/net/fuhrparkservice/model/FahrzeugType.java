package net.fuhrparkservice.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "tbl_FahrzeugType")
public class FahrzeugType extends AbstractBusinessObject {

	@Embedded
	private Model model;
	@Column(precision = 4)
	private long ps = 100;
	@Column(precision = 3)
	private long maxKmH;

	public FahrzeugType() {
	}

	public FahrzeugType(Model model, long ps, long maxKmH) {
		super();
		this.model = model;
		this.ps = ps;
		this.maxKmH = maxKmH;
	}

	public long getPs() {
		return ps;
	}

	public void setPs(long ps) {
		this.ps = ps;
	}

	public long getMaxKmH() {
		return maxKmH;
	}

	public void setMaxKmH(long maxKmH) {
		this.maxKmH = maxKmH;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof FahrzeugType) {
			FahrzeugType other = (FahrzeugType) obj;
			return this.getModel().equals(other.getModel());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getModel().hashCode();
	}

	@Override
	public String toString() {
		return getModel().toString();
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Model getModel() {
		return model;
	}

}
