package net.fuhrparkservice.model;

import javax.persistence.Entity;

@Entity
public class LKW extends FahrzeugType {

	private int maxZuladung;

	public LKW() {
		super();
	}

	public LKW( Model model, long ps, long maxKmH, int maxZuladung) {
		super( model, ps, maxKmH);
		this.maxZuladung = maxZuladung;
	}

	public int getMaxZuladung() {
		return maxZuladung;
	}

	public void setMaxZuladung(int maxZuladung) {
		this.maxZuladung = maxZuladung;
	}
}
