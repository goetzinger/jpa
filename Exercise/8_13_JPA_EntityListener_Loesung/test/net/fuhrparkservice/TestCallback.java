package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.listener.FahrzeugTypeListener;

import org.junit.Test;

public class TestCallback extends AbstractJPATestCase {

	@Override
	public void setUp() throws Exception {
		
	}

	@Test public void testCallbackFailed() {
		FahrzeugType fahrzeug = new LKW("1", new Model("VW", "Golf"), 120, 200,10000);
		manager.persist(fahrzeug);
		assertFalse(fahrzeug.isCalled());
	}

	@Test public void testCallback() {
		FahrzeugType fahrzeug = new LKW("1", new Model("VW", "Golf"), 120, 200,10000);
		manager.persist(fahrzeug);
		manager.flush();
		assertTrue(fahrzeug.isCalled());
		assertTrue(FahrzeugTypeListener.gewonnen);
	}
}
