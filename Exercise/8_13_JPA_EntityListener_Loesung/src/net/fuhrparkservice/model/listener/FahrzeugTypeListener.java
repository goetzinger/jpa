package net.fuhrparkservice.model.listener;

import javax.persistence.PostPersist;

import net.fuhrparkservice.model.FahrzeugType;

public class FahrzeugTypeListener {

	public static boolean gewonnen = false;
	
	@PostPersist
	public void doAfterInsert(Object inserted)
	{
		if (inserted instanceof FahrzeugType) {
			FahrzeugType fahrzeug = (FahrzeugType) inserted;
			gewonnen = !fahrzeug.isCalled();
		}
	}

}
