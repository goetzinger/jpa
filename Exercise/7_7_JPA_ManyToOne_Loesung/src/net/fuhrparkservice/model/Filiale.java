package net.fuhrparkservice.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_Filiale")
public class Filiale {

	@Id
	private String id;
	@Column(nullable = false, length = 100)
	private String ort;
	@OneToMany(mappedBy = "standort",cascade = CascadeType.PERSIST)
	private Set<Fahrzeug> fahrzeuge = new HashSet<Fahrzeug>();

	public Filiale() {
	}

	public Filiale(String id, String ort) {
		this.id = id;
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}

	@Override
	public String toString() {
		return this.ort;
	}

	public Object getId() {
		return this.id;
	}

	public void addFahrzeug(Fahrzeug fahrzeugItem) {
		fahrzeuge.add(fahrzeugItem);
		if (!this.equals(fahrzeugItem.getStandort()))
			fahrzeugItem.setStandort(this);
	}

}
