package net.fuhrparkservice.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_Filiale")
public class Filiale extends AbstractBusinessObject{

	@Column(nullable = false, length = 100)
	private String ort;
	@OneToMany(mappedBy="standort")
	private Set<FahrzeugItem> fahrzeuge = new HashSet<FahrzeugItem>();

	public Filiale() {
	}

	public Filiale(String id, String ort) {
		super(id);
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<FahrzeugItem> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<FahrzeugItem> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}

	@Override
	public String toString() {
		return this.ort;
	}


}
