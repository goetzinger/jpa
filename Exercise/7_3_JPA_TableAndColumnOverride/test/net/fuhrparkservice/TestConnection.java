package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	private FahrzeugType fahrzeugType;

	@Override
	public void setUp() throws Exception {

		fahrzeugType = new FahrzeugType("VW", "Golf", 120, 200);
		manager.persist(fahrzeugType);
		// TODO Add TestData with EntityManager
		manager.flush();
		manager.clear();
	}

	@Test
	public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class,
				fahrzeugType.getId()).getId());
	}

	@Test
	public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(null);
	}

	@Test
	public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(null);
	}

}
