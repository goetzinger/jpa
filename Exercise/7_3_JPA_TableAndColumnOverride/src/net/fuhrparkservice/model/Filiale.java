package net.fuhrparkservice.model;

import java.util.Set;

public class Filiale {

	private long id;
	private String ort;
	private Set<Fahrzeug> fahrzeuge;
	
	public Filiale() {
	}
	
	public Filiale( String ort)
	{
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}
	
	@Override
	public String toString() {
		return this.ort;
	}

}
