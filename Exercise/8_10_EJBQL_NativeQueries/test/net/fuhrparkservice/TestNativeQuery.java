package net.fuhrparkservice;

import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Kunde;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.PKW;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestNativeQuery extends AbstractJPATestCase {

	private Fahrzeug fahrzeugItem;

	public void setUp() throws Exception {
		FahrzeugType fahrzeug = new PKW(new Model("VW", "Golf"), 120, 200, 2);
		manager.persist(fahrzeug);
		FahrzeugType fahrzeug2 = new LKW(new Model("Mercedes", "10to"), 120,
				200, 10000);
		manager.persist(fahrzeug2);
		FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
		manager.persist(fahrzeug3);
		Filiale filiale = new Filiale("Muenchen");
		fahrzeugItem = new Fahrzeug(filiale, fahrzeug);
		Filiale stuttgart = new Filiale("Stuttgart");
		fahrzeugItem.setStandort(stuttgart);
		manager.persist(fahrzeugItem);
		Fahrzeug fahrzeugItem2 = new Fahrzeug(stuttgart, fahrzeug3);
		manager.persist(fahrzeugItem2);
		manager.persist(new Filiale("Koeln"));
		Kunde kunde = new Kunde(new Person("Hans", "Mustermann"));
		manager.persist(kunde);
		Kunde kunde2 = new Kunde(new Person("Franz", "Mueller"));
		manager.persist(kunde2);
		manager.persist(new Kunde(new Person("Herbert", "Schmitt")));
		manager.persist(new Kunde(new Person("Ingo", "Meyer")));
		manager.persist(new Kunde(new Person("Mathias", "Mayer")));
		manager.persist(new Kunde(new Person("Michael", "Anstaedt")));
		manager.persist(new Kunde(new Person("Ralf", "Gross")));
		
		manager.flush();
		manager.clear();
	}

	@Test public void testNativeQueryOfKunde() {
		// TODO Lade alle Kunden- und Personendaten mittels Native Query
	}

	@Test public void testNativeQueryOfFahrzeugUndItem() {
		// TODO Lade alle Fahrzeuge mit Fabrikat BMW und zugehˆrige FahrzeugItem
		// mittels Native Query und ResultSetMapping
	}

}
