select 	fahrzeugty0_.id as id0_, 
		fahrzeugty0_.maxKmH as maxKmH0_, 
		fahrzeugty0_.fabrikat as fabrikat0_, 
		fahrzeugty0_.modell as modell0_, 
		fahrzeugty0_.ps as ps0_, 
		fahrzeugty0_1_.maxZuladung as maxZulad1_5_, 
		fahrzeugty0_2_.tueren as tueren6_, 
		case when fahrzeugty0_1_.id is not null then 1 when fahrzeugty0_2_.id is not null then 2 when fahrzeugty0_.id is not null then 0 end as clazz_ 
		from tbl_FahrzeugType fahrzeugty0_ 
		left outer join TBL_LKW fahrzeugty0_1_ on fahrzeugty0_.id=fahrzeugty0_1_.id 
		left outer join TBL_PKW fahrzeugty0_2_ on fahrzeugty0_.id=fahrzeugty0_2_.id