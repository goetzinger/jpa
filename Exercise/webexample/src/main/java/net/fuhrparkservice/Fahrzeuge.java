package net.fuhrparkservice;

import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.fuhrparkservice.model.FahrzeugType;

@Path("fahrzeug")
public class Fahrzeuge {

	@Inject
	private FahrzeugService service;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JsonArray all() {
		List<FahrzeugType> fahrzeuge = service.getAll();
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (FahrzeugType fahrzeugType : fahrzeuge) {

			JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
			objectBuilder.add("model", fahrzeugType.getModell());
			objectBuilder.add("fabrikat", fahrzeugType.getFabrikat());
			arrayBuilder.add(objectBuilder.build());
		}
		return arrayBuilder.build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void add(JsonObject fahrzeug){
		FahrzeugType newOne = new FahrzeugType();
		newOne.setFabrikat(fahrzeug.getString("fabrikat"));
		newOne.setModell(fahrzeug.getString("model"));
		service.add(newOne);
		System.out.println("got it " + fahrzeug);
	}
}
