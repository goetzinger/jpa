package net.fuhrparkservice;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import net.fuhrparkservice.model.FahrzeugType;

@Transactional
public class FahrzeugService {

	@PersistenceContext
	private EntityManager manager;

	public List<FahrzeugType> getAll(){
		return manager.createQuery("SELECT f FROM FahrzeugType f", FahrzeugType.class).getResultList();
	}

	public void add(FahrzeugType newOne) {
		manager.persist(newOne);
		manager.flush();
	}
}
