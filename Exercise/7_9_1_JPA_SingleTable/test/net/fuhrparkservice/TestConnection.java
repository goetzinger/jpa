package net.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.Person;

import org.junit.Test;

public class TestConnection extends AbstractJPATestCase {

	private FahrzeugType lkw;
	private Filiale filialeMuenchen;
	private Fahrzeug fahrzeugItem;
	private Nutzer nutzer;
	private Filiale stuttgart;

	@Override
	public void setUp() throws Exception {

		lkw = new LKW(new Model("VW", "Golf"), 120, 200, 10000);
		manager.persist(lkw);
		filialeMuenchen = new Filiale("Muenchen");
		fahrzeugItem = new Fahrzeug(filialeMuenchen, lkw);
		stuttgart = new Filiale("Stuttgart");
		fahrzeugItem.setStandort(stuttgart);
		manager.persist(fahrzeugItem);
		nutzer = new Nutzer(new Person("Hans", "Mustermann"));
		manager.persist(nutzer);
		manager.flush();
		manager.clear();
	}

	@Test
	public void testFindFahrzeug() {
		assertNotNull(super.manager.find(FahrzeugType.class, lkw.getId())
				.getId());
	}

	@Test
	public void testFindNutzer() {
		// TODO find Nutzer with EntityManager
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId()).getId());
	}

	@Test
	public void testFindFiliale() {
		// TODO find filiale with EntityManager
		assertNotNull(super.manager
				.find(Filiale.class, filialeMuenchen.getId()).getId());
	}

	@Test
	public void testFindModel() {
		assertNotNull(super.manager
				.find(FahrzeugType.class, lkw.getId()).getModel()
				.getFabrikat());
	}

	@Test
	public void testFindPersonByNutzer() {
		assertNotNull(super.manager.find(Nutzer.class, nutzer.getId())
				.getPerson().getVorname());
	}

	@Test
	public void testOneToManyOfFiliale() {
		assertTrue(super.manager.find(Filiale.class, stuttgart.getId())
				.getFahrzeuge().toArray().length > 0);
	}

	@Test
	public void testManyToOneFahrzeugItem() {
		assertNotNull(super.manager.find(Fahrzeug.class, fahrzeugItem.getId())
				.getStandort());
	}

	@Test
	public void testManyToManyFahrzeugItem() {
		assertTrue(super.manager.find(Fahrzeug.class, fahrzeugItem.getId())
				.getStandortHistory().size() > 0);
	}

	@Test
	public void testInheritance() {
		assertNotNull(super.manager.find(LKW.class, lkw.getId()));
	}
}
